// @ts-check

import globals from "globals";
import eslint from "@eslint/js";
import tseslint from "typescript-eslint";
import pluginVue from "eslint-plugin-vue";
import playwright from "eslint-plugin-playwright";
import eslintConfigPrettier from "eslint-config-prettier";
import pluginPromise from "eslint-plugin-promise";

export default tseslint.config(
  {
    ignores: [
      "*/",
      "!e2e",
      "!server",
      "!src",
      "!shared",
      "!tests",
      "*config*s",
      "tests/vitest-setup.ts",
    ],
  },
  eslint.configs.recommended,
  ...tseslint.configs.strictTypeChecked,
  ...tseslint.configs.stylisticTypeChecked,
  {
    languageOptions: {
      parserOptions: {
        project: [
          "./tsconfig.client.json",
          "./tsconfig.server.json",
          "./e2e/tsconfig.json",
        ],
        tsconfigRootDir: import.meta.dirname,
      },
    },
  },
  {
    ...playwright.configs["flat/recommended"],
    files: ["e2e/**/*.{test,spec}.{js,ts,jsx,tsx}"],
  },
  ...pluginVue.configs["flat/recommended"],
  {
    languageOptions: {
      parserOptions: {
        parser: "@typescript-eslint/parser",
        extraFileExtensions: [".vue"],
      },
    },
    rules: {
      "@typescript-eslint/consistent-type-assertions": [
        "error",
        {
          assertionStyle: "as",
          objectLiteralTypeAssertions: "never",
        },
      ],
      "@typescript-eslint/no-shadow": "error",
      "@typescript-eslint/no-unused-vars": [
        "error",
        {
          argsIgnorePattern: "^_",
          varsIgnorePattern: "^_",
          caughtErrorsIgnorePattern: "^_",
        },
      ],
      "@typescript-eslint/restrict-template-expressions": [
        "error",
        {
          allowNumber: true,
        },
      ],
      "@typescript-eslint/use-unknown-in-catch-callback-variable": "warn",
    },
  },
  {
    files: ["src/**/*.vue"],
    rules: {
      "vue/multi-word-component-names": "off",
      "vue/component-name-in-template-casing": "error",
      "vue/component-options-name-casing": "error",
      "vue/custom-event-name-casing": "error",
      "vue/define-emits-declaration": "error",
      "vue/define-macros-order": "error",
      "vue/define-props-declaration": "error",
      //"vue/html-button-has-type": "error",
      "vue/match-component-file-name": "error",
      "vue/match-component-import-name": "error",
      "vue/new-line-between-multi-line-property": "error",
      "vue/next-tick-style": "error",
      //"vue/no-bare-strings-in-template": "error",
      "vue/no-boolean-default": "error",
      "vue/no-duplicate-attr-inheritance": "error",
      "vue/no-empty-component-block": "error",
      "vue/no-multiple-objects-in-class": "error",
      "vue/no-potential-component-option-typo": "error",
      //"vue/no-ref-object-destructure": "warn",
      "vue/no-required-prop-with-default": "error",
      "vue/no-restricted-block": "error",
    },
    languageOptions: {
      globals: {
        ...globals.browser,
      },
    },
  },
  eslintConfigPrettier,
  pluginPromise.configs["flat/recommended"],
);
