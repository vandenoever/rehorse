import { PageAnnotations } from "../../shared/annotations";

export interface Canvas {
  rect: DOMRect;
  pageWidthPt: number;
  pageHeightPt: number;
  annotations: PageAnnotations;
}
