import { ref } from "vue";

export const online = ref(navigator.onLine);
const updatOnlineStatus = (e: Event) => {
  online.value = e.type === "online";
};
window.addEventListener("online", updatOnlineStatus);
window.addEventListener("offline", updatOnlineStatus);
