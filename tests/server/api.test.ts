import request from "supertest";
import { createApp, initStorage } from "../../server/app.js";
import { loadConfig } from "../../server/config.js";
import { groups_io } from "../../shared/group.js";
import { io } from "../../shared/rehorse.js";
import { assert, describe, it, expect } from "vitest";

const configPath = "tests/data/basic/config.json";
const config = loadConfig(configPath);
config.data_dir = "tests/data/basic/data";
await initStorage(config);
const app = createApp(configPath, config);

describe("Groups endpoint", () => {
  it("should return the groups.", async () => {
    const res = await request(app).get("/api/groups").send();
    expect(res.statusCode).toEqual(200);
    const body = groups_io.decode(res.body);
    assert(body.isOk);
    expect(body.value[0]?.name).toBe("Assai");
    expect(body.value[1]?.name).toBe("Belcanto");
  });
});

describe("Group data", () => {
  it("should return the group data file.", async () => {
    const res = await request(app).get("/groups/assai/rehorse.json").send();
    expect(res.statusCode).toEqual(200);
    const body = io.decode(res.body);
    assert(body.isOk);
  });
});
