import { describe, it, expect } from "vitest";
import { annotationsIO, PartAnnotations } from "../../shared/annotations";
import { createId } from "../../shared/ids";

describe("IO", () => {
  it("can encode annotations", () => {
    const annotations: PartAnnotations = {
      pageAnnotations: [
        { items: [{ type: "path", id: createId(), d: [], color: "black" }] },
      ],
    };
    annotationsIO.encode(annotations);
    expect(true);
  });
});
