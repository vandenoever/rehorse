import { vi, describe, it, expect, beforeEach } from "vitest";
import { setActivePinia, createPinia } from "pinia";
import { useRehorseStore } from "../../src/store";

describe("Rehorse Store", () => {
  beforeEach(() => {
    setActivePinia(createPinia());
  });
  it("updates", async () => {
    const store = useRehorseStore();
    await vi.waitFor(() => {
      expect(store.groups.isLoading).toBe(false);
    });
    expect(store.groups.isError).toBe(true);
  });
});
