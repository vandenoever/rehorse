import { describe, it, expect } from "vitest";
import { mount, RouterLinkStub } from "@vue/test-utils";
import { createPinia, Pinia } from "pinia";
import UserButton from "../../../src/components/UserButton.vue";
import { useRehorseStore } from "../../../src/store";
import { cookies, logout } from "../../../src/user";
import jwt from "jsonwebtoken";
import { nextTick } from "vue";

const createWrapper = () => {
  const pinia: Pinia = createPinia();
  return {
    wrapper: mount(UserButton, {
      global: {
        plugins: [pinia],
        stubs: {
          RouterLink: RouterLinkStub,
        },
      },
    }),
    pinia,
  };
};

describe("UserButton", () => {
  it("not logged in", () => {
    const { wrapper } = createWrapper();
    expect(wrapper.text()).toBe("Log in");
  });
  it("logged in", async () => {
    const payload = {
      uid: 0,
      name: "Jane Doe",
      username: "jane",
      email: "jane@doe.doe",
    };
    const signedPayload = {
      ...payload,
      token: jwt.sign(payload, "secret", { expiresIn: "30d" }),
    };
    cookies.set("auth", signedPayload);
    const { wrapper, pinia } = createWrapper();
    expect(wrapper.text()).toBe("Jane Doe");
    useRehorseStore(pinia);
    logout();
    await nextTick();
    expect(wrapper.text()).toBe("Log in");
  });
});
