{
  description = "Rehorse";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs?ref=nixos-24.11";
    utils.url = "github:numtide/flake-utils";
  };
  outputs = { self, nixpkgs, utils }:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        package_json = builtins.fromJSON (pkgs.lib.readFile ./package.json);
        runtimeDeps = with pkgs; [ ffmpeg_6-headless git pdftk file ];
        gitInfoEnv = {
          GIT_LATEST_COMMIT_DATE = self.sourceInfo.lastModifiedDate;
          GIT_LATEST_COMMIT_SHA1 = if (self ? rev) then self.rev else "dirty";
        };
        nodejs = pkgs.nodejs_20; # nix always runs the package with nodejs_20
        # the overrides have no effect on the chosen executable
        rehorse = (pkgs.buildNpmPackage.override { inherit nodejs; }) ({
          inherit nodejs;
          pname = "rehorse";
          version = package_json.version;
          src = ./.;
          npmDepsHash = "sha256-GmPRu4Dtcyi/ZZrD0/s9L26BPDHRU5P82IpB3Xlis8Q=";
          nativeBuildInputs = [ pkgs.pkg-config ];
          buildInputs = [ pkgs.pango ];
          makeWrapperArgs =
            [ "--suffix PATH : ${pkgs.lib.strings.makeBinPath runtimeDeps}" ];
        } // gitInfoEnv);
      in rec {
        # `nix build`
        packages.rehorse = rehorse;
        packages.default = packages.rehorse;
        packages.prefetch-npm-deps = pkgs.prefetch-npm-deps;

        nixosModules.default =
          import ./rehorse_module.nix packages.default "rehorse";
        nixosModules.rehorse-dev =
          import ./rehorse_module.nix packages.default "rehorse-dev";

        # `nix run`
        apps.rehorse = utils.lib.mkApp { drv = packages.rehorse; };
        apps.default = apps.rehorse;

        devShells.default = pkgs.mkShell ({
          nativeBuildInputs = with pkgs;
            [ prefetch-npm-deps poppler_utils oxipng nixfmt-classic xvfb-run ]
            ++ runtimeDeps ++ [ nodejs sqlite-interactive ];
          PLAYWRIGHT_BROWSERS_PATH = pkgs.playwright-driver.browsers;
          PLAYWRIGHT_SKIP_BROWSER_DOWNLOAD = 1;
          shellHook = ''
            export PATH="$PATH:node_modules/.bin";
          '';
        } // gitInfoEnv);
      }) // {

        # a container for testing
        nixosConfigurations.container = nixpkgs.lib.nixosSystem rec {
          system = "x86_64-linux";
          modules = [
            self.nixosModules.${system}.default
            ({ config, pkgs, ... }: {
              boot.isContainer = true;
              networking.hostName = "rehorse-test-container";
              networking.firewall.allowedTCPPorts =
                [ config.services.rehorse.listenPort ];
              services.rehorse = {
                enable = true;
                listenAddress = "0.0.0.0";
              };
              system.stateVersion = "22.11";
            })
          ];
        };
      };
}
