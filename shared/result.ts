// Copyright (c) 2017 Chris Krycho
// SPDX-FileCopyrightText: 2023 Jos van den Oever <rehorse@vandenoever.info>
//
// SPDX-License-Identifier: AGPL-3.0-only

declare const Tag: unique symbol;

export interface Unit {
  [Tag]: true;
}

function createUnit(): Unit {
  const unit = { __proto__: null };
  assertUnit(unit);
  return unit;
}
// eslint-disable-next-line @typescript-eslint/no-empty-function
function assertUnit(_object: object): asserts _object is Unit {}
export const Unit = createUnit();

export interface Ok<T> {
  readonly variant: "Ok";
  isOk: true;
  isErr: false;
  value: T;
}
export interface Err<E> {
  readonly variant: "Err";
  isOk: false;
  isErr: true;
  value: E;
}
export declare type Result<T, E> = Ok<T> | Err<E>;
export function ok<T, E>(value: T): Result<T, E> {
  return {
    variant: "Ok",
    isOk: true,
    isErr: false,
    value,
  };
}
export function err<T, E>(value: E): Result<T, E> {
  return {
    variant: "Err",
    isOk: false,
    isErr: true,
    value,
  };
}
