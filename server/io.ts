// SPDX-FileCopyrightText: 2023 Jos van den Oever <rehorse@vandenoever.info>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { promises as fs } from "fs";
import { Result, Unit, err, ok } from "../shared/result.js";
import { Rehorse, sortRehorse, io } from "../shared/rehorse.js";
import {
  annotationsIO,
  PageAnnotations,
  PartAnnotations,
} from "../shared/annotations.js";

export async function loadRehorse(
  dir: string,
): Promise<Result<[string, Rehorse], Error>> {
  const rehorsepath = dir + "/rehorse.json";
  let json: string;
  try {
    json = await fs.readFile(rehorsepath, "utf8");
  } catch (e) {
    return err(new Error(`Could not read ${rehorsepath}`, { cause: e }));
  }
  const result = io.decode(JSON.parse(json));
  if (result.isErr) {
    return result;
  }
  sortRehorse(result.value);
  return ok([json, result.value]);
}

export async function loadAnnotations(
  path: string,
): Promise<Result<PageAnnotations, Error>> {
  let json: string;
  try {
    json = await fs.readFile(path, "utf8");
  } catch (_e) {
    json = '{"pageAnnotations":[{"items":[]}]}';
  }
  const result = annotationsIO.decode(JSON.parse(json));
  if (result.isErr) {
    return result;
  }
  const pageAnnotations = result.value.pageAnnotations[0];
  if (!pageAnnotations || result.value.pageAnnotations.length !== 1) {
    return err(
      new Error(
        `Unexpected number of pages present (${result.value.pageAnnotations.length})`,
      ),
    );
  }
  return ok(pageAnnotations);
}

export async function saveAnnotations(
  path: string,
  annotations: PartAnnotations,
): Promise<Result<Unit, Error>> {
  const result = annotationsIO.decode(annotations);
  if (result.isErr) {
    return result;
  }
  const json = JSON.stringify(annotations, null, "  ");
  await fs.writeFile(path, json);
  return ok(Unit);
}
