import { Request, Response } from "express";
import {
  ErrorLogType,
  EmptyType,
  AddMembersType,
  Annotations,
  wrapServer,
  NamesType,
  Return,
} from "../shared/api.js";
import { Config, User } from "./config.js";
import { err, ok } from "../shared/result.js";
import { log_frontend } from "./logging-frontend.js";
import { getNames } from "./util.js";
import { broadcast } from "./websocket.js";
import { addMembers } from "./members.js";
import { saveGroupConfig, storeAnnotations } from "./storage.js";
import { Group } from "../shared/group.js";
import { isId, PartId } from "../shared/ids.js";

interface Context {
  configPath: string;
  config: Config;
  user?: User;
}

// eslint-disable-next-line @typescript-eslint/require-await
async function logError(
  context: Context,
  error: ErrorLogType,
): Return<EmptyType> {
  if (context.user) {
    log_frontend(context.user.username, error);
  }
  return ok({});
}

// eslint-disable-next-line @typescript-eslint/require-await
async function names(context: Context, _input: EmptyType): Return<NamesType> {
  if (context.user) {
    return ok(getNames(context.config.users));
  }
  return ok({});
}

async function addMembersFn(
  context: Context,
  msg: AddMembersType,
): Return<EmptyType> {
  if (context.user) {
    await addMembers(context.configPath, context.config, context.user, msg);
    broadcast({ type: "group-update" });
  }
  return ok({});
}

// eslint-disable-next-line @typescript-eslint/require-await
async function saveGroup(context: Context, group: Group): Return<EmptyType> {
  if (context.user) {
    saveGroupConfig(context.configPath, context.config, context.user, group);
    broadcast({ type: "group-update" });
  }
  return ok({});
}

async function saveAnnotations(
  context: Context,
  annotations: Annotations,
): Return<EmptyType> {
  if (!isId<PartId>(annotations.partId)) {
    return err(new Error("partid is not an id"));
  }
  const result = await storeAnnotations(
    context.config.data_dir,
    annotations.group,
    annotations.arrangementId,
    annotations.partId,
    annotations.partAnnotations,
  );
  return result;
}

type Handler = (
  req: Request,
  res: Response<never, { user: User }>,
) => Promise<void>;

function wrap(f: (context: Context, input: unknown) => Return<string>) {
  return (context: Context): Handler =>
    async (req, res) => {
      context.user = res.locals.user;
      const result = await f(context, req.body);
      if (result.isErr) {
        res.writeHead(400);
        res.end(result.value);
        return;
      }
      const json = result.value;
      const head: Record<string, string> = {
        "Content-Type": "application/json",
      };
      res.writeHead(200, head);
      res.end(json);
    };
}

export const endpoints = wrapServer(wrap, {
  logError,
  names,
  addMembers: addMembersFn,
  saveGroup,
  saveAnnotations,
});
