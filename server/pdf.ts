// SPDX-FileCopyrightText: 2023 Jos van den Oever <rehorse@vandenoever.info>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { promises as fs } from "fs";
import { Result, ok, err } from "../shared/result.js";
import { cmd, cmdBin } from "./cmd.js";
import { ArrangementPart } from "../shared/rehorse.js";
import { ArrangementId } from "../shared/ids.js";

const RE_NUMBER_OF_PAGES = /^NumberOfPages: (\d+)$/m;

export async function countPdfPages(
  path: string,
): Promise<Result<number, Error>> {
  const result = await cmd("pdftk", [path, "dump_data"]);
  if (result.isErr) {
    return err(new Error(result.value));
  }
  const match = RE_NUMBER_OF_PAGES.exec(result.value)?.[1];
  if (!match) {
    return err(
      new Error(`The number of pages is not known. (${result.value})`),
    );
  }
  const numberOfPages = parseInt(match, 10);
  if (numberOfPages > 0) {
    return ok(numberOfPages);
  }
  return err(new Error(`The number of pages (${match}) is not known.`));
}

async function isFile(path: string): Promise<boolean> {
  try {
    const stat = await fs.stat(path);
    return stat.isFile();
  } catch (_e) {
    return false;
  }
}

export async function cutPages(
  dir: string,
  arrangementId: ArrangementId,
  part: ArrangementPart[],
): Promise<Result<Buffer, Error>> {
  const args: string[] = [];
  for (const p of part) {
    const path = `${dir}/arrangements/${arrangementId}/pdf/${p.pdf}`;
    let rot = "";
    if (p.rotation === 90) {
      rot = "east";
    } else if (p.rotation === 180) {
      rot = "south";
    } else if (p.rotation === 270) {
      rot = "west";
    }
    args.push(path, "cat", `${p.start}-${p.end}${rot}`);
  }
  args.push("output", "-");
  const result = await cmdBin("pdftk", args);
  if (result.isErr) {
    return err(new Error(result.value));
  }
  return ok(result.value);
}

export async function stampPdf(
  pdf: Buffer,
  stampPath: string,
): Promise<Result<Buffer, Error>> {
  const doStamp = await isFile(stampPath);
  if (!doStamp) {
    return ok(pdf);
  }
  const args = ["-", "stamp", stampPath, "output", "-"];
  const result = await cmdBin("pdftk", args, pdf);
  if (result.isErr) {
    return err(new Error(result.value));
  }
  return ok(result.value);
}
