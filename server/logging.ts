import express from "express";
import morgan, { TokenIndexer } from "morgan";
import http from "http";
import winston, { Logger } from "winston";
import { createSqliteTransport } from "./logging-sqlite.js";

const { combine, timestamp, json } = winston.format;

export const logger: Logger = createLogger();
function createLogger() {
  return winston.createLogger({
    level: "http",
    format: combine(timestamp(), json()),
    transports: [new winston.transports.Console(), createSqliteTransport()],
  });
}

function jsonFormat<
  Request extends http.IncomingMessage = http.IncomingMessage,
  Response extends http.ServerResponse = http.ServerResponse,
>(tokens: TokenIndexer<Request, Response>, req: Request, res: Response) {
  const simpleKeys = [
    "http-version",
    "method",
    "referrer",
    "remote-addr",
    "remote-user",
    "response-time",
    "status",
    "total-time",
    "url",
    "user-agent",
  ];
  const obj: Record<string, unknown> = {};
  for (const key of simpleKeys) {
    if (tokens[key]) {
      obj[key] = tokens[key](req, res);
    }
  }
  if (tokens["date"]) {
    obj["time"] = tokens["date"](req, res, "iso");
  }
  if (tokens["res"]) {
    obj["content-length"] = tokens["res"](req, res, "content-length");
  }

  return JSON.stringify(obj);
}

export function addLogging(app: express.Router) {
  const morganMiddleware = morgan(jsonFormat, {
    stream: {
      // Configure Morgan to use our custom logger with the http severity
      write: (message: string) => logger.http(JSON.parse(message)),
    },
  });

  app.use(morganMiddleware);
}
