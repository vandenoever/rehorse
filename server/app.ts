// SPDX-FileCopyrightText: 2023 Jos van den Oever <rehorse@vandenoever.info>
//
// SPDX-License-Identifier: AGPL-3.0-only

import cookieParser from "cookie-parser";
import express, { Request, Response, NextFunction } from "express";
import {
  RequestHandlerParams,
  ParamsDictionary,
  Query,
} from "express-serve-static-core";
import { FileHandle } from "fs/promises";
import jwt, { JwtPayload } from "jsonwebtoken";
import type { User } from "../shared/cookie.js";
import version from "../shared/version.json" with { type: "json" };
import { io as cookie_io } from "../shared/cookie.js";
import { validate as uuidValidate, version as uuidVersion } from "uuid";
import { hashPassword, Config, User as ConfigUser } from "./config.js";
import {
  initGroup,
  getAnnotations,
  getGroupAudio,
  getPdf,
  getPart,
  getGroupRehorse,
  saveGroupRehorse,
  saveAudio,
  savePdf,
  checkFiles,
  getRehorseSha256s,
} from "./storage.js";
import type { Pdf, Piece, Rehorse } from "../shared/rehorse.js";
import { groups_io } from "../shared/group.js";
import type { ArrangementId, PartId, PieceId, Sha256 } from "../shared/ids.js";
import { isId, isSha256 } from "../shared/ids.js";
import { logger, addLogging } from "./logging.js";
import { addWebSocket, broadcast } from "./websocket.js";
import { requestPasswordReset, resetPassword } from "./password.js";
import { endpoints } from "./api.js";

const expiresIn = "30d";

// turn async handlers into sync handlers
const ash =
  <
    A1 extends ParamsDictionary,
    A2,
    A3,
    A4 extends Query,
    A5 extends Record<string, object>,
    B1,
    B2 extends Record<string, object>,
  >(
    func: (
      areq: Request<A1, A2, A3, A4, A5>,
      ares: Response<B1, B2>,
    ) => Promise<void>,
  ) =>
  (
    req: Request<A1, A2, A3, A4, A5>,
    res: Response<B1, B2>,
    next: NextFunction,
  ) => {
    // after the async function finishes, call next
    func(req, res)
      .then(() => {
        // eslint-disable-next-line promise/no-callback-in-promise
        next();
        return;
      })
      .catch((err: unknown) => {
        // eslint-disable-next-line promise/no-callback-in-promise
        next(err);
      });
  };

function isAuthenticated(
  config: Config,
  username: string,
  password: string,
): User | undefined {
  const user = config.users.find((u) => u.username === username);
  if (!user) {
    return undefined;
  }
  const hash = hashPassword(user.salt, password);
  if (hash !== user.passwordSha512) {
    return undefined;
  }
  return {
    uid: user.uid,
    name: user.name,
    username: user.username,
    email: user.email,
  };
}

function setToken(res: Response<never>, accessToken: unknown) {
  res.cookie("auth", accessToken, {
    httpOnly: false,
    sameSite: "strict",
  });
}

function createToken(config: Config, user: User): User {
  const payload = user;
  const token = jwt.sign(payload, config.jwt_secret, { expiresIn });
  payload.token = token;
  return payload;
}

// a token where the user is not logged in
function setEmptyToken(res: Response<never>) {
  setToken(res, undefined);
}

async function initStorage(config: Config) {
  for (const group of config.groups) {
    await initGroup(config.data_dir, group.groupname);
  }
  await checkFiles(config);
}

function filenameFromHeaders(
  headers: Record<string, string | string[] | undefined>,
): string {
  const header = headers["content-disposition"];
  if (
    !(
      typeof header === "string" &&
      header.startsWith('attachment; filename="') &&
      header.endsWith('"')
    )
  ) {
    return "";
  }
  const quoted = header.slice(22, -1);
  return quoted.replaceAll('\\"', '"');
}

async function getAudio(
  data: string,
  prefix: string,
  req: Request,
  res: Response,
) {
  const group = req.params["group"] ?? "";
  const sha256 = req.params["sha256"];
  if (!sha256 || !isSha256<PieceId>(sha256)) {
    res.status(500).end();
    return;
  }
  try {
    const [size, contentType, handle] = await getGroupAudio(
      data,
      group,
      prefix,
      sha256,
    );
    const head: Record<string, string> = {
      "Content-Type": contentType,
      // needed for seeking for some formats on khtml family browsers
      "Accept-Ranges": "bytes",
      "Cache-Control": "public, max-age=31557600",
    };
    writeStream(res, head, size, handle);
  } catch (e) {
    logger.error(e);
  }
}

function writeStream(
  res: Response,
  head: Record<string, string>,
  size: number,
  handle: FileHandle,
) {
  head["Content-Length"] = size.toString();
  res.writeHead(200, head);
  const stream = handle.createReadStream();
  stream.pipe(res);
}

function checkUuid(
  _req: Request,
  res: Response,
  next: NextFunction,
  id: string,
) {
  if (uuidValidate(id) && uuidVersion(id) === 4) {
    next();
  } else {
    res.sendStatus(404);
  }
}

function setupApp(configPath: string, config: Config, app: express.Router) {
  const jsonBody: RequestHandlerParams<never, never, never> = express.json({
    limit: "20mb",
  });
  const rawBody: RequestHandlerParams<never, never, Buffer> = express.raw({
    limit: "20mb",
    type: "*/*",
  });
  app.use(cookieParser());
  // if the token is not valid, set an empty token in the response
  function getUserFromAuthCookie(
    req: Request,
    res: Response<never, { user: ConfigUser | undefined }>,
    next: NextFunction,
  ) {
    const cookies = req.cookies;
    const auth_cookie: unknown = cookies["auth"];
    if (auth_cookie === undefined) {
      next();
      return;
    }
    const result = cookie_io.decode(auth_cookie);
    if (result.isErr) {
      setEmptyToken(res);
      next();
      return;
    }
    const cookie = result.value;
    if (cookie.token === undefined) {
      setEmptyToken(res);
      next();
      return;
    }
    let parsed_token: string | JwtPayload;
    try {
      parsed_token = jwt.verify(cookie.token, config.jwt_secret);
    } catch (e) {
      logger.error(e);
      setEmptyToken(res);
      next();
      return;
    }
    const result2 = cookie_io.decode(parsed_token);
    if (result2.isErr) {
      setEmptyToken(res);
      next();
      return;
    }
    const cookieUser = result2.value;
    const user = config.users.find((u) => u.username === cookieUser.username);
    if (!user) {
      setEmptyToken(res);
      next();
      return;
    }
    res.locals.user = user;
    next();
  }
  app.use(getUserFromAuthCookie);
  function verifyToken(
    _req: Request,
    res: Response<never, { user: ConfigUser | undefined }>,
    next: NextFunction,
  ) {
    if (!res.locals.user) {
      res.status(401).end("No valid user.");
      return;
    }
    next();
  }

  app.get("/api/version", (_req, res) => {
    const head: Record<string, string> = {
      "Content-Type": "application/json",
    };
    res.writeHead(200, head);
    res.end(JSON.stringify(version));
  });

  for (const [name, endpoint] of Object.entries(endpoints)) {
    app.post(
      `/api/${name}`,
      jsonBody,
      verifyToken,
      ash(endpoint({ configPath, config })),
    );
  }

  app.post(
    "/api/password-reset",
    jsonBody,
    ash(
      async (
        req: Request<ParamsDictionary, void, { token: string }>,
        res: Response,
      ): Promise<void> => {
        if ("token" in req.body && typeof req.body.token === "string") {
          await resetPassword(config, req.body.token);
        }
        const head: Record<string, string> = {
          "Content-Type": "application/json",
        };
        res.writeHead(200, head);
        res.end(JSON.stringify({}));
      },
    ),
  );

  app.post(
    "/api/request-password-reset",
    jsonBody,
    ash(
      async (
        req: Request<ParamsDictionary, void, { email: string }>,
        res: Response,
      ): Promise<void> => {
        if ("email" in req.body && typeof req.body.email === "string") {
          await requestPasswordReset(config, req.body.email);
        }
        const head: Record<string, string> = {
          "Content-Type": "application/json",
        };
        res.writeHead(200, head);
        res.end(JSON.stringify({}));
      },
    ),
  );

  app.get("/api/groups", (_req, res) => {
    const json = groups_io.encode(config.groups);
    const head: Record<string, string> = {
      "Content-Type": "application/json",
    };
    res.writeHead(200, head);
    res.end(json);
  });

  app.param("group", (_req, res, next, id) => {
    const group = config.groups.find((g) => g.groupname === id);
    if (group) {
      next();
    } else {
      res.sendStatus(404);
    }
  });

  app.param("sha256", function (_req, res, next, id: string) {
    if (isSha256<PieceId>(id)) {
      next();
    } else {
      res.sendStatus(404);
    }
  });

  app.param("arrangement", checkUuid);
  app.param("part", checkUuid);

  app.get(
    "/groups/:group/rehorse.json",
    ash((req: Request<{ group: string }>, res): Promise<void> => {
      const result = getGroupRehorse(req.params.group);
      if (result.isErr) {
        logger.error(result.value);
        res.sendStatus(500);
      } else {
        const head: Record<string, string> = {
          "Content-Type": "application/json",
        };
        res.writeHead(200, head);
        res.end(result.value.content);
      }
      return Promise.resolve();
    }),
  );
  app.put(
    "/groups/:group/rehorse.json",
    jsonBody,
    verifyToken,
    ash(
      async (
        req: Request<{ group: string }, never, Rehorse>,
        res: Response<never, { user: ConfigUser }>,
      ): Promise<void> => {
        const group = req.params.group;
        if (!group) {
          res.sendStatus(500);
          return;
        }
        const user = res.locals.user;
        const result = await saveGroupRehorse(
          config.data_dir,
          user,
          group,
          req.body,
        );
        if (result.isErr) {
          logger.error(result.value);
          res.sendStatus(500);
        } else {
          broadcast({ type: "update", rehorseJsonSha256: getRehorseSha256s() });
          res.status(200).end();
        }
      },
    ),
  );

  app.get(
    "/groups/:group/audio/opus/:sha256",
    ash(async (req, res) => {
      await getAudio(config.data_dir, "opus/", req, res);
    }),
  );

  app.get(
    "/groups/:group/audio/mp3/:sha256",
    ash(async (req, res) => {
      await getAudio(config.data_dir, "mp3/", req, res);
    }),
  );

  app.get(
    "/groups/:group/audio/:sha256",
    ash(async (req, res) => {
      await getAudio(config.data_dir, "", req, res);
    }),
  );

  app.get(
    "/groups/:group/arrangement/:arrangement/pdf/:sha256",
    // TODO verifyToken,
    ash(
      async (
        req: Request<{
          group: string;
          arrangement: ArrangementId;
          sha256: Sha256;
        }>,
        res: Response,
        // TODO: res: Response<never, { user: ConfigUser }>
      ) => {
        const group = req.params.group;
        const arrangement = req.params.arrangement;
        const sha256 = req.params.sha256;
        const head: Record<string, string> = {
          "Content-Type": "application/pdf",
          "Cache-Control": "public, max-age=31557600",
        };
        const [size, handle] = await getPdf(
          config.data_dir,
          group,
          arrangement,
          sha256,
        );
        writeStream(res, head, size, handle);
      },
    ),
  );

  app.get(
    "/groups/:group/arrangement/:arrangement/part/:part/:sha256",
    // TODO verifyToken,
    ash(
      async (
        req: Request<{
          group: string;
          arrangement: ArrangementId;
          part: PartId;
        }>,
        res: Response,
        // TODO: res: Response<never, { user: ConfigUser }>
      ) => {
        const group = req.params.group;
        const arrangement = req.params.arrangement;
        const part = req.params.part;
        if (!isId<ArrangementId>(arrangement) || !isId<PartId>(part)) {
          res.status(500).end();
          return;
        }
        const result = await getPart(config.data_dir, group, arrangement, part);
        if (result.isErr) {
          logger.error(result.value);
          res.status(500).end(result.value.toString());
          return;
        }
        const pdf = result.value;
        const head: Record<string, string> = {
          "Content-Type": "application/pdf",
          "Content-Length": pdf.length.toString(),
          "Cache-Control": "public, max-age=31557600",
        };
        res.writeHead(200, head);
        res.end(pdf);
      },
    ),
  );

  app.get(
    "/groups/:group/arrangement/:arrangement/annotations/:part",
    // TODO verifyToken,
    ash(
      async (
        req: Request<{
          group: string;
          arrangement: ArrangementId;
          part: string;
        }>,
        res: Response,
        // TODO: res: Response<never, { user: ConfigUser }>
      ) => {
        const group = req.params.group;
        const arrangement = req.params.arrangement;
        const part = req.params.part;
        if (!isId<ArrangementId>(arrangement) || !isId<PartId>(part)) {
          res.status(500).end();
          return;
        }
        const result = await getAnnotations(
          config.data_dir,
          group,
          arrangement,
          part,
        );
        if (result.isErr) {
          logger.error(result.value);
          res.status(500).end(result.value.toString());
          return;
        }
        res.status(200).json(result.value);
      },
    ),
  );

  // upload audio file
  app.post(
    "/groups/:group/audio",
    rawBody,
    verifyToken,
    ash(
      async (
        req: Request<{ group: string }, [string, Piece], Buffer>,
        res: Response<[string, Piece], { user: ConfigUser }>,
      ): Promise<void> => {
        const group = req.params.group;
        const user = res.locals.user;
        const filename = filenameFromHeaders(req.headers);
        if (!filename) {
          res.status(400).end("Missing filename");
          return;
        }
        if (!group) {
          res.sendStatus(500);
          return;
        }
        const result = await saveAudio(
          config.data_dir,
          user,
          group,
          filename,
          req.body,
        );
        if (result.isErr) {
          logger.error(result.value);
          res.status(500).end(result.value.toString());
          return;
        }
        res.status(200).json(result.value);
      },
    ),
  );

  // upload pdf file
  app.post(
    "/groups/:group/arrangement/:arrangement/pdf",
    rawBody,
    verifyToken,
    ash(
      async (
        req: Request<
          { group: string; arrangement: ArrangementId },
          Pdf,
          Buffer
        >,
        res: Response<Pdf, { user: ConfigUser }>,
      ) => {
        const group = req.params.group;
        const arrangement = req.params.arrangement;
        const user = res.locals.user;
        const filename = filenameFromHeaders(req.headers);
        if (!filename || !isId<ArrangementId>(arrangement)) {
          res.status(400).end("Missing filename");
          return;
        }
        if (!group) {
          res.sendStatus(500);
          return;
        }
        const result = await savePdf(
          config.data_dir,
          user,
          group,
          arrangement,
          filename,
          req.body,
        );
        if (result.isErr) {
          logger.error(result.value);
          res.status(500).end(result.value.toString());
          return;
        }
        res.status(200).json(result.value);
      },
    ),
  );

  app.post(
    "/login",
    jsonBody,
    (
      req: Request<
        never,
        { token: User; success: boolean } | { status: number; message: string },
        { username: string; password: string }
      >,
      res,
    ) => {
      const { username, password } = req.body;
      const user = isAuthenticated(config, username, password);
      if (!user) {
        const status = 401;
        const message = "Incorrect username or password";
        res.status(status).json({ status, message });
        return;
      }
      const accessToken = createToken(config, user);
      setToken(res, accessToken);
      res.status(200).json({ token: accessToken, success: true });
    },
  );

  // read data from cwd and application files from the install directory
  // do not send last-modified, because on nixos it is always 1970
  // express.static always sends weak ETags, so disable them.
  // When index.html or sw.js change, the ETag often stays the same.
  // With this configuration, files are always sent.
  const staticOptionsNoETag = { lastModified: false, etag: false };
  const distpath = config.www_dir ?? "dist/www";
  app.use("/", express.static(distpath, staticOptionsNoETag));
}

function createApp(configPath: string, config: Config): express.Express {
  const app = express();
  app.set("etag", "strong");
  addLogging(app);
  const url = new URL(config.url_prefix);
  if (url.search) {
    throw new Error("url_prefix in config should not contain a query (?)");
  }
  if (url.pathname === "/") {
    setupApp(configPath, config, app);
  } else {
    const router = express.Router();
    setupApp(configPath, config, router);
    app.use(url.pathname, router);
  }
  return app;
}

function runApp(app: express.Express, bind: string, port: number) {
  const server = app.listen(port, bind);
  addWebSocket(server, () => {
    return { type: "update", rehorseJsonSha256: getRehorseSha256s() };
  });
}

export { initStorage, createApp, runApp };
