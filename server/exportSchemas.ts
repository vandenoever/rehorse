import { rehorseJsonSchema } from "../shared/rehorse.js";
import { groupsJsonSchema } from "../shared/group.js";
import { promises as fs } from "fs";
import path from "path";
import { configJsonSchema } from "./config.js";
import { cookieJsonSchema } from "../shared/cookie.js";
import { annotationsJsonSchema } from "../shared/annotations.js";

export async function exportSchemas(onlyCheck: boolean) {
  const schemas: Record<string, () => string> = {
    "config.schema.json": configJsonSchema,
    "cookie.schema.json": cookieJsonSchema,
    "rehorse.schema.json": rehorseJsonSchema,
    "groups.schema.json": groupsJsonSchema,
    "annotations.schema.json": annotationsJsonSchema,
  };
  let changed = false;
  const prefix = "schemas";
  for (const [file, fn] of Object.entries(schemas)) {
    const p = path.join(prefix, file);
    let currentContent: unknown;
    try {
      currentContent = await fs.readFile(p, "utf8");
    } catch (_e) {
      currentContent = undefined;
    }
    const content = fn();
    if (currentContent !== content) {
      if (onlyCheck) {
        changed = true;
        console.error(`${p} has changed.`);
      } else {
        await fs.writeFile(p, content);
      }
    }
  }
  process.exit(+changed);
}
//await exportSchemas(false);
