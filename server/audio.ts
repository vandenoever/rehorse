// SPDX-FileCopyrightText: 2023 Jos van den Oever <rehorse@vandenoever.info>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { Result, ok, err } from "../shared/result.js";
import { cmd } from "./cmd.js";

export enum AudioType {
  Unknown,
  Opus,
  Mp3,
  Vorbis,
}

export async function getMimetype(
  filepath: string,
  type: AudioType,
): Promise<Result<string, string>> {
  if (type === AudioType.Opus) {
    return ok("audio/ogg; codecs=opus");
  } else if (type === AudioType.Vorbis) {
    return ok("audio/ogg; codecs=vorbis");
  } else if (type === AudioType.Mp3) {
    return ok("audio/mpeg");
  }
  const result = await cmd("file", ["--brief", "--mime-type", filepath]);
  if (result.isErr) {
    return result;
  }
  return ok(result.value.trim());
}

export async function getMetadata(
  filepath: string,
): Promise<Result<Metadata, string>> {
  const result = await cmd("ffprobe", [
    "-v",
    "quiet",
    "-show_format",
    "-show_streams",
    "-print_format json",
    filepath,
  ]);
  if (result.isErr) {
    return result;
  }
  const metadata = JSON.parse(result.value) as Metadata;
  cleanTags(metadata);
  return ok(metadata);
}

function cleanTags(metadata: Metadata) {
  const tags1 = metadata.format.tags;
  const tags2 = metadata.streams[0]?.tags;
  const tags: Record<string, string> = {};
  if (typeof tags1 === "object") {
    for (const [key, value] of Object.entries(tags1)) {
      if (typeof value === "string") {
        tags[key.toLowerCase()] = value;
      }
    }
  }
  if (typeof tags2 === "object") {
    for (const [key, value] of Object.entries(tags2)) {
      if (typeof value === "string") {
        tags[key.toLowerCase()] = value;
      }
    }
  }
  metadata.tags = tags;
  metadata.format.tags = undefined;
  if (metadata.streams[0]) {
    metadata.streams[0].tags = undefined;
  }
  return tags;
}

interface Stream {
  codec_name: string;
  codec_type: string;
  duration: string;
  tags: object | undefined;
}

interface Format {
  format_name: string;
  duration: string;
  tags: object | undefined;
}

export interface Metadata {
  streams: Stream[];
  format: Format;
  tags: Record<string, string>;
}

export function audioTypeFromMetadata(metadata: Metadata): AudioType {
  const audio_stream = metadata.streams.find((s) => s.codec_type === "audio");
  if (!audio_stream) {
    return AudioType.Unknown;
  }
  const format = metadata.format.format_name;
  const codec = audio_stream.codec_name;
  if (format === "ogg" && codec === "opus") {
    return AudioType.Opus;
  }
  if (format === "ogg" && codec === "vorbis") {
    return AudioType.Vorbis;
  }
  if (format === "mp3" && codec === "mp3") {
    return AudioType.Mp3;
  }
  return AudioType.Unknown;
}

/// return duration rounded to 100 milliseconds
export function durationFromMetadata(metadata: Metadata): number {
  let duration = parseFloat(metadata.format.duration);
  if (!(duration > 0)) {
    duration = parseFloat(metadata.streams[0]?.duration ?? "");
  }
  if (duration > 0) {
    return Math.round(10 * duration) / 10;
  }
  return 0;
}

export async function transcode(
  filepath: string,
  codec: string,
): Promise<Result<string, string>> {
  const targetfile = filepath + "." + codec;
  let lib;
  if (codec === "mp3") {
    lib = "libmp3lame";
  } else if (codec === "opus") {
    lib = "libopus";
  } else {
    return err("Unsupported format.");
  }
  const result = await cmd("ffmpeg", [
    "-hide_banner",
    "-nostats",
    "-loglevel",
    "quiet",
    "-y",
    "-i",
    filepath,
    "-vn",
    "-map_metadata",
    "0",
    "-c:a",
    lib,
    "-ac",
    "2",
    targetfile,
  ]);
  if (result.isOk) {
    return ok(targetfile);
  }
  return result;
}
