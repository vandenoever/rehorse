import DatabaseConstructor from "better-sqlite3";

function createLogger() {
  const db = new DatabaseConstructor("frontend-errors.sqlite");
  const cols = [
    "id INTEGER PRIMARY KEY",
    "timestamp INTEGER DEFAULT (strftime('%s','now'))",
    "user TEXT",
    "value BLOB",
  ];
  const table = "frontend_errors";
  db.exec(`CREATE TABLE IF NOT EXISTS ${table} (${cols.join(", ")})`);
  const insert = db.prepare(
    `INSERT INTO ${table} (user, value) VALUES (@user, @info)`,
  );
  return { db, insert };
}

const logger = createLogger();
export function log_frontend(user: string, info: unknown) {
  console.log(user, info);
  try {
    logger.insert.run({ user, info: JSON.stringify(info) });
  } catch (e) {
    console.error(e);
  }
}
