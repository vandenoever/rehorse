import crypto from "crypto";
import { find_group } from "../shared/group.js";
import { isId, isLcName, PartId, UserId } from "../shared/ids.js";
import {
  changeUserPassword,
  Config,
  saveConfigToPath,
  User,
} from "./config.js";
import { mail } from "./mail.js";
import { EMAIL_RE } from "../shared/email.js";

export interface InvitesOptions {
  groupname: string;
  members: object[];
  message: string;
}

export interface InviteOptions {
  groupname: string;
  members: object[];
  name: string;
  email: string;
  role: string | undefined;
  message: string;
}

export async function addMembers(
  configPath: string,
  config: Config,
  user: User,
  options: InvitesOptions,
) {
  for (const member of options.members) {
    if (
      !("name" in member) ||
      typeof member.name !== "string" ||
      !("email" in member) ||
      typeof member.email !== "string" ||
      !EMAIL_RE.test(member.email)
    ) {
      console.error("Error while adding member", member, user);
      continue;
    }
    try {
      await addMember(configPath, config, user, {
        ...options,
        name: member.name,
        email: member.email.toLowerCase(),
        role:
          "role" in member && typeof member.role === "string"
            ? member.role
            : undefined,
      });
    } catch (e) {
      console.error("Error while adding member", member, user, e);
    }
  }
}
export function createPassword(): string {
  return crypto.randomBytes(6).toString("base64");
}
async function addMember(
  configPath: string,
  config: Config,
  user: User,
  options: InviteOptions,
) {
  const group = find_group(config.groups, options.groupname);
  if (!group) {
    throw new Error(`${options.groupname} is not a valid group`);
  }
  const loginUrl = (group.url_prefix ?? config.url_prefix) + "#/user";
  if (!group.admins.includes(user.username)) {
    throw new Error(`${user.username} is not an admin of ${options.groupname}`);
  }
  let invitedUser = config.users.find((u) => {
    return u.email === options.email;
  });
  let pwd;
  if (invitedUser === undefined) {
    pwd = createPassword();
    invitedUser = createUser(config, options.name, options.email, pwd);
    group.members.push(invitedUser.username);
  } else if (!group.members.includes(invitedUser.username)) {
    group.members.push(invitedUser.username);
  }
  let roleMsg = "";
  if (options.role && isId<PartId>(options.role)) {
    const part = group.parts.get(options.role);
    if (part && !part.members.includes(invitedUser.username)) {
      part.members.push(invitedUser.username);
      roleMsg = ` as ${part.name}`;
    }
  }
  let fullMessage = `Hello ${options.name},

You have been added to ${group.name}${roleMsg}.

${options.message}
`;
  if (pwd) {
    fullMessage += `

You can log in at ${loginUrl} with:
  username: ${invitedUser.username}
  password: ${pwd}
`;
  } else {
    fullMessage += `

You can log in at ${loginUrl} with your Rehorse account.
`;
  }
  saveConfigToPath(configPath, config);
  await mail(config, {
    from: user.email,
    to: invitedUser.email,
    subject: `${user.name} added you to ${group.name}`,
    message: fullMessage,
  });
}

function emailUsername(email: string): string {
  const index = email.indexOf("@");
  if (index !== -1) {
    return email.substring(0, index);
  }
  return email;
}

function createUser(
  config: Config,
  name: string,
  email: string,
  password: string,
): User {
  let usernamePrefix = emailUsername(email).replaceAll(/[^a-z0-9]+/g, "");
  if (!isLcName<UserId>(usernamePrefix)) {
    usernamePrefix = "user";
    if (!isLcName<UserId>(usernamePrefix)) {
      throw new Error("Cannot create username");
    }
  }
  let n = 0;
  let username: UserId = usernamePrefix;
  while (config.users.some((u) => u.username === username)) {
    n += 1;
    const u = `${usernamePrefix}${n}`;
    if (!isLcName<UserId>(u)) {
      throw new Error("Cannot create username");
    }
    username = u;
  }
  const user = {
    uid: 1 + Math.max(...config.users.map((u) => u.uid)),
    name: name || username,
    username,
    email,
    salt: "",
    passwordSha512: "",
  };
  changeUserPassword(user, password);
  config.users.push(user);
  return user;
}
