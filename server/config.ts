// SPDX-FileCopyrightText: 2023 Jos van den Oever <rehorse@vandenoever.info>
//
// SPDX-License-Identifier: AGPL-3.0-only

import fs from "fs";
import { join as path_join, dirname } from "path";
import crypto from "crypto";
import { Result, Unit, ok, err } from "../shared/result.js";
import { groupSchema } from "../shared/group.js";
import { create_io } from "../shared/ajv.js";
import { createId, lcNameSchema, PartId, userIdSchema } from "../shared/ids.js";
import { Type, StaticDecode } from "@sinclair/typebox";
import { ajvToJsonSchema } from "../shared/ajv.js";
import { dateTimeSchema } from "../shared/agenda.js";

const passwordResetRequestSchema = Type.Object({
  requestDate: dateTimeSchema,
  token: Type.String(),
});
const userSchema = Type.Object(
  {
    uid: Type.Integer(),
    name: Type.String({ minLength: 1 }),
    username: userIdSchema,
    email: Type.String({ format: "email" }),
    salt: Type.String({ pattern: "^[0-9a-f]{32}$" }),
    passwordSha512: Type.String({ pattern: "^[0-9a-f]{128}$" }),
    passwordResetRequest: Type.Optional(passwordResetRequestSchema),
  },
  { additionalProperties: false },
);

const mailConfigSchema = Type.Object({
  from: Type.String(),
  smtp_host: Type.String(),
  username: Type.String(),
  password: Type.String(),
});
export type MailConfig = StaticDecode<typeof mailConfigSchema>;

const configSchema = Type.Object(
  {
    users: Type.Array(userSchema, { minItems: 1, uniqueItems: true }),
    groups: Type.Array(groupSchema, { minItems: 1, uniqueItems: true }),
    data_dir: Type.Optional(Type.String()),
    www_dir: Type.Optional(Type.String()),
    url_prefix: Type.String({ format: "url", pattern: "/$" }),
    jwt_secret: Type.String(),
    mail: mailConfigSchema,
  },
  { additionalProperties: false },
);

export type User = StaticDecode<typeof userSchema>;
type ConfigWithOptionalDataDir = StaticDecode<typeof configSchema>;

export type Config = ConfigWithOptionalDataDir & {
  data_dir: NonNullable<ConfigWithOptionalDataDir["data_dir"]>;
};

export const configJsonSchema = () =>
  ajvToJsonSchema(
    {
      $schema: "http://json-schema.org/draft-07/schema#",
      $id: "https://rehorse.vandenoever.info/config.schema.json",
      title: "Config",
      description: "The configuration of Rehorse",
    },
    configAjvSchema,
  );

const configAjvSchema = {
  ...configSchema,
  $defs: { lcname: lcNameSchema },
};

export const io = create_io(configAjvSchema, [lcNameSchema]);

export function validate(
  input: unknown,
): Result<ConfigWithOptionalDataDir, Error> {
  const result = io.decode(input);
  if (result.isErr) {
    return err(new Error("config is not valid", { cause: result.value }));
  }
  const config = result.value;
  const missingUsers = config.groups
    .flatMap((group) => {
      return group.admins
        .filter((a) => !config.users.some((u) => u.username === a))
        .concat(
          group.members.filter(
            (m) => !config.users.some((u) => u.username === m),
          ),
        );
    })
    .filter((user) => user)
    .filter((value, index, array) => array.indexOf(value) === index);
  if (missingUsers.length !== 0) {
    return err(
      new Error(`Not all users are defined: ${missingUsers.join(",")}.`),
    );
  }
  return ok(config);
}

export function loadConfig(path: string): Config {
  const json: string = fs.readFileSync(path, "utf8");
  const result = validate(JSON.parse(json));
  if (result.isErr) {
    throw result.value;
  }
  return {
    ...result.value,
    data_dir: result.value.data_dir ?? path_join(dirname(path), "data"),
  };
}

export function saveConfigToPath(
  configPath: string,
  config: ConfigWithOptionalDataDir,
): Result<Unit, Error> {
  const json: string = io.encode(config);
  const result = validate(JSON.parse(json));
  if (result.isErr) {
    return result;
  }
  fs.writeFileSync(configPath, json);
  return ok(Unit);
}

export function saveConfig(config: Config): Result<Unit, Error> {
  return saveConfigToPath("config.json", config);
}

export function changeUserPassword(user: User, password: string) {
  user.salt = crypto.randomBytes(16).toString("hex");
  user.passwordSha512 = hashPassword(user.salt, password);
}

export function changePassword(
  config: Config,
  username: string,
  password: string,
): Result<Unit, Error> {
  const user = config.users.find((u) => u.username == username);
  if (user) {
    changeUserPassword(user, password);
    return saveConfig(config);
  } else {
    return err(new Error(`User '${username}' is not defined.`));
  }
}

export function hashPassword(salt: string, password: string): string {
  return crypto.pbkdf2Sync(password, salt, 1000, 64, `sha512`).toString(`hex`);
}

export function addPart(
  config: Config,
  groupname: string,
  partname: string,
): Result<Unit, Error> {
  const group = config.groups.find((g) => g.groupname == groupname);
  if (group) {
    const id = createId<PartId>();
    group.parts.set(id, { name: partname, members: [], backups: [] });
    return saveConfig(config);
  } else {
    return err(new Error(`Group '${groupname}' is not defined.`));
  }
}
