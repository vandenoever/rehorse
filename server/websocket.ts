import http from "http";
import WebSocket, { WebSocketServer } from "ws";
import {
  server_msg,
  ServerMessage,
  UpdateMessage,
} from "../shared/websocket.js";

const locals = {
  clients: new Set<WebSocket>(),
};

function addWebSocket(server: http.Server, onConnection: () => UpdateMessage) {
  const webSocketServer = new WebSocketServer({ server });
  webSocketServer.on("error", console.error);
  webSocketServer.on("connection", (client: WebSocket) => {
    console.info("Total connected clients:", webSocketServer.clients.size);
    client.send(server_msg.encode(onConnection()));
    // This is here until groups information also has a sha256
    client.send(server_msg.encode({ type: "group-update" }));
    locals.clients = webSocketServer.clients;
  });
}

function broadcast(msg: ServerMessage) {
  const encoded = server_msg.encode(msg);
  locals.clients.forEach((client: WebSocket) => {
    client.send(encoded);
  });
}

export { addWebSocket, broadcast };
