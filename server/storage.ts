// SPDX-FileCopyrightText: 2023 Jos van den Oever <rehorse@vandenoever.info>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { join as path_join } from "path";
import { promises as fs, constants as fs_constants } from "fs";
import crypto from "crypto";
import {
  User,
  Config,
  io as config_io,
  validate,
  saveConfigToPath,
} from "./config.js";
import {
  io,
  Rehorse,
  Piece,
  Pdf,
  ArrangementPart,
  sortRehorse,
} from "../shared/rehorse.js";
import { PageAnnotations, PartAnnotations } from "../shared/annotations.js";
import type { ArrangementId, PartId, PieceId, Sha256 } from "../shared/ids.js";
import { isSha256 } from "../shared/ids.js";
import { loadRehorse, loadAnnotations, saveAnnotations } from "./io.js";
import {
  audioTypeFromMetadata,
  durationFromMetadata,
  getMetadata,
  getMimetype,
  transcode,
  AudioType,
  Metadata,
} from "./audio.js";
import { countPdfPages, cutPages, stampPdf } from "./pdf.js";
import { initGit, saveToGit, GitCommit } from "./git.js";
import { Result, Unit, ok, err } from "../shared/result.js";
import { find_group, Group, replace_group } from "../shared/group.js";

function getGroupDir(dataDir: string, group: string): string {
  return path_join(dataDir, "groups", group);
}

export async function initGroup(dataDir: string, group: string) {
  const dir = getGroupDir(dataDir, group);
  await initGit(dir);
}

interface RehorseCacheEntry {
  rehorse: Rehorse;
  content: string;
  sha256: Sha256;
}

const groupRehorseCache = new Map<string, RehorseCacheEntry>();

export function getRehorseSha256s(): Record<string, string> {
  const map: Record<string, string> = {};
  for (const [group, value] of groupRehorseCache) {
    map[group] = value.sha256;
  }
  return map;
}

export function getGroupRehorse(
  group: string,
): Result<RehorseCacheEntry, Error> {
  const cached = groupRehorseCache.get(group);
  if (cached) {
    return ok(cached);
  }
  return err(new Error(`There is not Rehorse for the group ${group}.`));
}

async function loadGroupRehorse(
  dataDir: string,
  group: string,
): Promise<Result<RehorseCacheEntry, Error>> {
  const cached = groupRehorseCache.get(group);
  if (cached) {
    return ok(cached);
  }
  const dir = getGroupDir(dataDir, group);
  const rehorse = await loadRehorse(dir);
  if (rehorse.isErr) {
    return rehorse;
  }
  const encoded = rehorse.value[0];
  const entry = {
    rehorse: rehorse.value[1],
    content: encoded,
    sha256: calculateSha256(encoded),
  };
  groupRehorseCache.set(group, entry);
  return ok(entry);
}

export function saveGroupConfig(
  configPath: string,
  config: Config,
  user: User,
  newGroup: Group,
): Result<Unit, Error> {
  const groupname = newGroup.groupname;
  const group = find_group(config.groups, groupname);
  if (!group?.admins.includes(user.username)) {
    return err(new Error(`${user.username} is not an admin of ${groupname}`));
  }
  replace_group(config.groups, newGroup);
  return saveConfig(configPath, config);
}

function saveConfig(configPath: string, config: Config): Result<Unit, Error> {
  const json = config_io.encode(config);
  const new_config = validate(JSON.parse(json));
  if (new_config.isErr) {
    return new_config;
  }
  return saveConfigToPath(configPath, new_config.value);
}

export async function saveGroupRehorse(
  dataDir: string,
  user: User,
  group: string,
  rehorse_data: unknown,
): Promise<Result<Unit, Error>> {
  const r = io.decode(rehorse_data);
  if (r.isErr) {
    return r;
  }
  const rehorse = r.value;
  sortRehorse(rehorse);
  const dir = getGroupDir(dataDir, group);
  return await commitRehorse(
    group,
    dir,
    {
      committer: user.name,
      email: user.email,
      message: "Saving json file",
      files: [],
    },
    rehorse,
  );
}

async function tempFileFromString(data: string): Promise<[string, string]> {
  const buffer = Buffer.from(data, "utf-8");
  return tempFileFromBuffer(buffer);
}

function calculateSha256(data: string | Buffer): Sha256 {
  const digest = crypto.createHash("sha256").update(data).digest("hex");
  if (isSha256(digest)) {
    return digest;
  }
  throw new Error("implementation error in calculating sha256");
}

async function tempFileFromBuffer(data: Buffer): Promise<[string, Sha256]> {
  const digest = calculateSha256(data);
  const dir = "data/tmp";
  await fs.mkdir(dir, { recursive: true });
  const filepath = path_join(dir, digest);
  await fs.writeFile(filepath, data, { encoding: "binary" });
  return [filepath, digest];
}

async function getFileSha256(filepath: string): Promise<Sha256> {
  const data = await fs.readFile(filepath);
  return calculateSha256(data);
}

async function tempFileFromFile(filepath: string): Promise<[string, Sha256]> {
  const digest = await getFileSha256(filepath);
  return [filepath, digest];
}

export async function getGroupAudio(
  dataDir: string,
  group: string,
  prefix: string,
  file: PieceId,
): Promise<[number, string, fs.FileHandle]> {
  const dir = getGroupDir(dataDir, group);
  let contentType = "";
  if (prefix === "opus/") {
    contentType = "audio/ogg; codecs=opus";
  } else if (prefix === "mp3/") {
    contentType = "audio/mpeg";
  } else {
    const r = getGroupRehorse(group);
    if (r.isOk) {
      const piece = r.value.rehorse.pieces.get(file);
      if (piece) {
        contentType = piece.mimetype;
      }
    }
  }
  const path = `${dir}/audio/${prefix}${file}`;
  const stat = await fs.stat(path);
  const handle = await fs.open(path);
  return [stat.size, contentType, handle];
}

export async function getPdf(
  dataDir: string,
  group: string,
  arrangement: ArrangementId,
  file: Sha256,
): Promise<[number, fs.FileHandle]> {
  const dir = getGroupDir(dataDir, group);
  const path = `${dir}/arrangements/${arrangement}/pdf/${file}`;
  const stat = await fs.stat(path);
  const handle = await fs.open(path);
  return [stat.size, handle];
}

function getArrangementPart(
  group: string,
  arrangementId: ArrangementId,
  partId: PartId,
): Result<ArrangementPart[], Error> {
  const r = getGroupRehorse(group);
  if (r.isErr) {
    return r;
  }
  const rehorse = r.value.rehorse;
  const arrangement = rehorse.arrangements.get(arrangementId);
  if (arrangement === undefined) {
    return err(new Error("No arrangement " + arrangementId));
  }
  const part = arrangement.parts.get(partId);
  if (part === undefined || part.length === 0) {
    return err(new Error("No part " + partId));
  }
  return ok(part);
}

async function getPartCacheKey(
  stampPath: string,
  part: ArrangementPart[],
): Promise<Sha256> {
  const input = {
    stampChecksum: await getFileSha256(stampPath),
    part,
  };
  return calculateSha256(JSON.stringify(input));
}

async function getCacheEntry(
  dataDir: string,
  cacheKey: Sha256,
): Promise<Buffer | undefined> {
  const cachePath = path_join(dataDir, "cache", cacheKey);
  try {
    const data = await fs.readFile(cachePath);
    return data;
  } catch {
    return;
  }
}

async function saveCacheEntry(
  dataDir: string,
  cacheKey: Sha256,
  data: Buffer,
): Promise<void> {
  const dir = path_join(dataDir, "cache");
  try {
    await fs.mkdir(dir, { recursive: true });
    await fs.writeFile(path_join(dir, cacheKey), data);
  } catch {
    return;
  }
}

export async function getPart(
  dataDir: string,
  group: string,
  arrangementId: ArrangementId,
  partId: PartId,
): Promise<Result<Buffer, Error>> {
  const r = getArrangementPart(group, arrangementId, partId);
  if (r.isErr) {
    return r;
  }
  const part = r.value;
  const dir = getGroupDir(dataDir, group);
  const stampPath = dir + "/stamp.pdf";
  const cacheKey = await getPartCacheKey(stampPath, part);
  const cacheEntry = await getCacheEntry(dataDir, cacheKey);
  if (cacheEntry) {
    return ok(cacheEntry);
  }
  const pdfResult = await cutPages(dir, arrangementId, part);
  if (pdfResult.isErr) {
    return pdfResult;
  }
  const stampResult = await stampPdf(pdfResult.value, stampPath);
  if (stampResult.isOk) {
    await saveCacheEntry(dataDir, cacheKey, stampResult.value);
  }
  return stampResult;
}

// Retrieve the paths for the json files with the annotations for
// each pdf page in the part.
// This list can be uses to store or retrieve these annotations.
function getPartAnnotationPaths(
  dataDir: string,
  group: string,
  arrangementId: ArrangementId,
  partId: PartId,
): Result<string[], Error> {
  const r = getArrangementPart(group, arrangementId, partId);
  if (r.isErr) {
    return r;
  }
  const dir = getGroupDir(dataDir, group);
  const part = r.value;
  const paths: string[] = [];
  const addPath = (pdf: string, i: number) => {
    paths.push(`${dir}/arrangements/${arrangementId}/pdf/${pdf}_${i}.json`);
  };
  for (const p of part) {
    if (p.start <= p.end) {
      for (let i = p.start; i <= p.end; ++i) {
        addPath(p.pdf, i);
      }
    } else {
      for (let i = p.start; i >= p.end; --i) {
        addPath(p.pdf, i);
      }
    }
  }
  return ok(paths);
}

export async function getAnnotations(
  dataDir: string,
  group: string,
  arrangementId: ArrangementId,
  partId: PartId,
): Promise<Result<PartAnnotations, Error>> {
  const r = getPartAnnotationPaths(dataDir, group, arrangementId, partId);
  if (r.isErr) {
    return r;
  }
  const paths = r.value;
  const pageAnnotations: PageAnnotations[] = [];
  for (const path of paths) {
    const a = await loadAnnotations(path);
    if (a.isOk) {
      pageAnnotations.push(a.value);
    } else {
      return a;
    }
  }
  return ok({ pageAnnotations });
}
export async function storeAnnotations(
  dataDir: string,
  group: string,
  arrangementId: ArrangementId,
  partId: PartId,
  annotations: PartAnnotations,
): Promise<Result<Unit, Error>> {
  const r = getPartAnnotationPaths(dataDir, group, arrangementId, partId);
  if (r.isErr) {
    return r;
  }
  const paths = r.value;
  if (paths.length !== annotations.pageAnnotations.length) {
    return err(
      new Error(
        `The lengths of the annotations (${annotations.pageAnnotations.length}) and the number of pages (${paths.length}) in the part do not match up.`,
      ),
    );
  }
  for (const [index, path] of paths.entries()) {
    const pageAnnotations = annotations.pageAnnotations[index];
    if (pageAnnotations) {
      await saveAnnotations(path, { pageAnnotations: [pageAnnotations] });
    }
  }
  return ok(Unit);
}

// commit changes but first change rehorse.json via the provided
// function
async function commitWithRehorse(
  group: string,
  dir: string,
  commit: GitCommit,
  f: (r: Rehorse) => void,
): Promise<Result<Unit, Error>> {
  const r = getGroupRehorse(group);
  if (r.isErr) {
    return r;
  }
  const rehorse = r.value.rehorse;
  f(rehorse);
  return await commitRehorse(group, dir, commit, rehorse);
}

async function commitRehorse(
  group: string,
  dir: string,
  commit: GitCommit,
  rehorse: Rehorse,
): Promise<Result<Unit, Error>> {
  const json = io.encode(rehorse);
  groupRehorseCache.set(group, {
    rehorse,
    content: json,
    sha256: calculateSha256(json),
  });
  const tmpRehorse = await tempFileFromString(json);
  commit.files.push({
    from: tmpRehorse[0],
    to: "rehorse.json",
  });
  await saveToGit(dir, commit);
  return ok(Unit);
}

function titleFromMetadata(metadata: Metadata, filename: string) {
  let title = metadata.tags["title"];
  if (!title) {
    return filename;
  }
  const composer = metadata.tags["composer"];
  if (composer) {
    title += " – " + composer;
  }
  const artist = metadata.tags["artist"];
  if (artist) {
    title += " – " + artist;
  }
  return title;
}

export async function saveAudio(
  dataDir: string,
  user: User,
  group: string,
  providedFilename: string,
  audio: Buffer,
): Promise<Result<[string, Piece], Error>> {
  const [tmpfilepath, digest] = await tempFileFromBuffer(audio);
  const files = [
    {
      from: tmpfilepath,
      to: "audio/" + digest,
    },
  ];
  const metadataResult = await getMetadata(tmpfilepath);
  if (metadataResult.isErr) {
    return err(new Error("Unsupported audio format"));
  }
  const metadata = metadataResult.value;
  const type = audioTypeFromMetadata(metadata);
  const mimetypeResult = await getMimetype(tmpfilepath, type);
  if (mimetypeResult.isErr) {
    return err(new Error("Could not determine file format"));
  }
  const mimetype = mimetypeResult.value;
  const duration = durationFromMetadata(metadata);
  if (!duration) {
    return err(new Error("Unknown duration"));
  }
  const piece: Piece = {
    title: titleFromMetadata(metadata, providedFilename),
    filename: providedFilename,
    mimetype,
    marks: [
      { name: "", time: 0 },
      { name: "", time: duration },
    ],
  };
  if (type !== AudioType.Opus) {
    const result = await transcode(tmpfilepath, "opus");
    if (result.isErr) {
      return err(new Error("Cannot convert to Opus"));
    }
    const [opuspath, opusdigest] = await tempFileFromFile(result.value);
    piece.opus = opusdigest;
    files.push({
      from: opuspath,
      to: "audio/opus/" + opusdigest,
    });
  }
  if (type !== AudioType.Mp3) {
    const result = await transcode(tmpfilepath, "mp3");
    if (result.isErr) {
      return err(new Error("Cannot convert to mp3"));
    }
    const [mp3path, mp3digest] = await tempFileFromFile(result.value);
    piece.mp3 = mp3digest;
    files.push({
      from: mp3path,
      to: "audio/mp3/" + mp3digest,
    });
  }
  const groupdir = getGroupDir(dataDir, group);
  const dir = groupdir + "/audio";
  const filepath = dir + "/" + digest;
  await fs.mkdir(dir, { recursive: true });
  await fs.writeFile(filepath, audio, { encoding: "binary" });
  await commitWithRehorse(
    group,
    groupdir,
    {
      committer: user.name,
      email: user.email,
      message: "Saving audio file",
      files,
    },
    (rehorse) => {
      if (isSha256<PieceId>(digest) && !rehorse.pieces.has(digest)) {
        rehorse.pieces.set(digest, piece);
      }
    },
  );
  return ok([digest, piece]);
}

export async function savePdf(
  dataDir: string,
  user: User,
  group: string,
  arrangement: ArrangementId,
  providedFilename: string,
  pdf: Buffer,
): Promise<Result<Pdf, Error>> {
  const [tmpfilepath, digest] = await tempFileFromBuffer(pdf);
  const groupdir = getGroupDir(dataDir, group);
  const suffix = "arrangements/" + arrangement + "/pdf";
  const dir = groupdir + "/" + suffix;
  const filepath = suffix + "/" + digest;
  await fs.mkdir(dir, { recursive: true });
  const pagesResult = await countPdfPages(tmpfilepath);
  if (pagesResult.isErr) {
    return pagesResult;
  }
  const pdfEntry = {
    sha256: digest,
    filename: providedFilename,
    pages: pagesResult.value,
  };
  await commitWithRehorse(
    group,
    groupdir,
    {
      committer: user.name,
      email: user.email,
      message: "Saving pdf file",
      files: [
        {
          from: tmpfilepath,
          to: filepath,
        },
      ],
    },
    (rehorse) => {
      const a = rehorse.arrangements.get(arrangement);
      if (a) {
        a.pdfs.push(pdfEntry);
      }
    },
  );
  return ok(pdfEntry);
}

const checkFile = async (file: string) => {
  return fs.access(
    file,
    fs_constants.F_OK | fs_constants.R_OK | fs_constants.W_OK,
  );
};

const checkGroupPdfFiles = async (groupDir: string, rehorse: Rehorse) => {
  const pdfPaths: string[] = [];
  const arrangementsDir = path_join(groupDir, "arrangements");
  rehorse.arrangements.forEach((arrangement, arrangementId) => {
    const pdfDir = path_join(arrangementsDir, arrangementId, "pdf");
    arrangement.pdfs.forEach((pdf) => {
      pdfPaths.push(path_join(pdfDir, pdf.sha256));
    });
  });
  const promises = pdfPaths.map(checkFile);
  return Promise.all(promises);
};

const checkGroupAudioFiles = async (groupDir: string, rehorse: Rehorse) => {
  const audioPaths: string[] = [];
  const audioDir = path_join(groupDir, "audio");
  rehorse.pieces.forEach((piece, key) => {
    audioPaths.push(path_join(audioDir, key));
    if (piece.mp3) {
      audioPaths.push(path_join(audioDir, "mp3", piece.mp3));
    }
    if (piece.opus) {
      audioPaths.push(path_join(audioDir, "opus", piece.opus));
    }
  });
  const promises = audioPaths.map(checkFile);
  return Promise.all(promises);
};

const checkGroupFiles = async (config: Config, group: Group) => {
  const result = await loadGroupRehorse(config.data_dir, group.groupname);
  if (result.isErr) {
    throw result.value;
  }
  const rehorse = result.value.rehorse;
  const groupDir = getGroupDir(config.data_dir, group.groupname);
  const pdfCheck = checkGroupPdfFiles(groupDir, rehorse);
  const audioCheck = checkGroupAudioFiles(groupDir, rehorse);
  return Promise.all([pdfCheck, audioCheck]);
};

export const checkFiles = async (config: Config) => {
  const promises = config.groups.map((group) => checkGroupFiles(config, group));
  await Promise.all(promises);
};
