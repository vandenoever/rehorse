import { User } from "./config.js";

export function getNames(users: User[]): Record<string, string> {
  const names: Record<string, string> = {};
  for (const user of users) {
    names[user.username] = user.name;
  }
  return names;
}
