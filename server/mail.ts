import nodemailer from "nodemailer";
import { Config } from "./config.js";
import { logger } from "./logging.js";

export interface MailOptions {
  from: string;
  to: string;
  subject: string;
  message: string;
}

export const mail = async (config: Config, options: MailOptions) => {
  const transporter = nodemailer.createTransport({
    host: config.mail.smtp_host,
    secure: true,
    auth: {
      user: config.mail.username,
      pass: config.mail.password,
    },
  });

  const mailOptions = {
    from: config.mail.from,
    replyTo: options.from,
    to: options.to,
    subject: options.subject,
    text: options.message,
  };
  const info = await transporter.sendMail(mailOptions);
  logger.info(info);
};
