#!/usr/bin/env node

// SPDX-FileCopyrightText: 2023 Jos van den Oever <rehorse@vandenoever.info>
//
// SPDX-License-Identifier: AGPL-3.0-only

import fs from "fs";
import path from "path";
import { addPart, changePassword, loadConfig, Config } from "./config.js";
import { initStorage, createApp, runApp } from "./app.js";
import { exportSchemas } from "./exportSchemas.js";

function setpwd(username: string | undefined, password: string | undefined) {
  if (!username) {
    console.error("No username was provided.");
    process.exit(1);
  }
  if (!password) {
    console.error("No password was provided.");
    process.exit(1);
  }
  const config: Config = loadConfig("config.json");
  const result = changePassword(config, username, password);
  if (result.isErr) {
    console.error(result);
    process.exit(1);
  } else {
    process.exit(0);
  }
}

function addpart(group: string, partname: string) {
  const config: Config = loadConfig("config.json");
  const result = addPart(config, group, partname);
  if (result.isErr) {
    console.error(result);
    process.exit(1);
  } else {
    process.exit(0);
  }
}

function unknownArgument(name: string) {
  console.error(`Unknown argument: '${name}'`);
  process.exit(1);
}

async function serveApp(argv: string[], scriptPath: string) {
  let bind = "0.0.0.0";
  let port = 8124;
  let configPath = "config.json";
  while (argv.length > 1) {
    const name = argv.shift() ?? "";
    const value = argv.shift() ?? "";
    if (typeof value !== "string") {
      continue;
    }
    if (name === "--bind") {
      bind = value;
    } else if (name == "--port") {
      port = parseInt(value, 10);
    } else if (name == "--config") {
      configPath = value;
    } else {
      unknownArgument(name);
    }
  }
  if (argv.length > 0) {
    unknownArgument(argv.pop() ?? "");
    process.exit(1);
  }
  const config = loadConfig(configPath);
  config.www_dir = path.resolve(path.join(scriptPath, "../../www"));
  if (!fs.existsSync(config.www_dir)) {
    config.www_dir = path.resolve(path.join(".", "dist/www"));
  }
  await initStorage(config);
  const app = createApp(configPath, config);
  runApp(app, bind, port);

  console.log(`Server running at ${bind}:${port}/`);
}

async function run(): Promise<void> {
  const argv = process.argv;
  argv.shift();
  const script = argv.shift();
  if (script === undefined) {
    throw new Error("The path of the script is not known.");
  }
  const cmd = argv.shift();
  if (cmd === "setpwd") {
    setpwd(argv.shift(), argv.shift());
  } else if (cmd === "setpart") {
    addpart(argv.shift() ?? "", argv.shift() ?? "");
  } else if (cmd === "exportSchemas") {
    await exportSchemas(argv.shift() === "--check");
    process.exit(0);
  } else {
    if (cmd) {
      argv.unshift(cmd);
    }
    await serveApp(argv, script);
  }
}
await run();
