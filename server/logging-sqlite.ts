import DatabaseConstructor, { Database, Statement } from "better-sqlite3";
import Transport from "winston-transport";
import TransportStream from "winston-transport";

class Sqlite3Transport extends Transport {
  db: Database;
  insert: Statement;
  constructor(
    db: Database,
    insert: Statement,
    options: TransportStream.TransportStreamOptions,
  ) {
    super(options);
    this.db = db;
    this.insert = insert;
  }
  override log(info: unknown, callback: () => void) {
    try {
      this.insert.run({ info: JSON.stringify(info) });
    } catch (e) {
      console.error(e);
    }
    callback();
  }
}
export function createSqliteTransport(): Transport {
  const db = new DatabaseConstructor("http-log.db");
  const cols = [
    "id INTEGER PRIMARY KEY",
    "timestamp INTEGER DEFAULT (strftime('%s','now'))",
    "value BLOB",
  ];
  const table = "http_log";
  db.exec(`CREATE TABLE IF NOT EXISTS ${table} (${cols.join(", ")})`);
  const insert = db.prepare(`INSERT INTO ${table} (value) VALUES (@info)`);
  return new Sqlite3Transport(db, insert, {});
}
