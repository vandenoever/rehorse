// SPDX-FileCopyrightText: 2023 Jos van den Oever <rehorse@vandenoever.info>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { spawn } from "child_process";
import { Result, ok, err } from "../shared/result.js";
import { logger } from "./logging.js";

export function cmdBin(
  command: string,
  args: string[],
  stdin?: Buffer,
): Promise<Result<Buffer, string>> {
  const p = spawn(command, args, { shell: true });
  if (stdin) {
    p.stdin.end(stdin);
  }
  const stdout: Buffer[] = [];
  let stderr = "";
  return new Promise((resolve) => {
    p.stdout.on("data", (x: Buffer) => {
      stdout.push(x);
    });
    p.stderr.on("data", (x: Buffer) => {
      logger.error(x.toString());
      stderr += x.toString();
    });
    p.on("exit", (code) => {
      let r: Result<Buffer, string>;
      if (code === 0) {
        r = ok(Buffer.concat(stdout));
      } else {
        r = err(stderr);
      }
      resolve(r);
    });
  });
}

export async function cmd(
  command: string,
  args: string[],
): Promise<Result<string, string>> {
  const result = await cmdBin(command, args);
  if (result.isErr) {
    return result;
  }
  return ok(result.value.toString());
}
