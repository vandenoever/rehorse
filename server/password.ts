import { v4 as uuidv4 } from "uuid";
import { changePassword, Config, saveConfig, User } from "./config.js";
import { mail } from "./mail.js";
import { createDateTime } from "../shared/agenda.js";
import { createPassword } from "./members.js";

export async function requestPasswordReset(config: Config, email: string) {
  const user = config.users.find((u) => u.email === email);
  if (!user) {
    return;
  }
  if (user.passwordResetRequest) {
    const oneHourAgo = createDateTime(-3600 * 1000);
    if (user.passwordResetRequest.requestDate > oneHourAgo) {
      console.log(`A password reset for ${email} was requested too soon.`);
      // too soon to ask again
      return;
    }
  }
  user.passwordResetRequest = {
    requestDate: createDateTime(0),
    token: uuidv4(),
  };
  await mailPasswordResetLink(config, user, user.passwordResetRequest.token);
  return saveConfig(config);
}

async function mailPasswordResetLink(
  config: Config,
  user: User,
  token: string,
) {
  await mail(config, {
    from: user.email,
    to: user.email,
    subject: "Password reset link",
    message: `A password reset was requested.
Visit this link to reset your password:
    ${config.url_prefix}#/reset-password/${token}

If you did not request a password reset, ignore this mail.
`,
  });
}

export async function resetPassword(config: Config, token: string) {
  const user = config.users.find(
    (u) => u.passwordResetRequest?.token === token,
  );
  if (!user) {
    return;
  }
  delete user.passwordResetRequest;
  const password = createPassword();
  changePassword(config, user.username, password);
  return mailNewPassword(config, user, password);
}

async function mailNewPassword(config: Config, user: User, password: string) {
  await mail(config, {
    from: user.email,
    to: user.email,
    subject: "Password was reset",
    message: `Your password was reset.
User: ${user.username}
Password: ${password}

Visit this link to log in:
    ${config.url_prefix}#/user

`,
  });
}
