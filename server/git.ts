// SPDX-FileCopyrightText: 2023 Jos van den Oever <rehorse@vandenoever.info>
//
// SPDX-License-Identifier: AGPL-3.0-only

import { simpleGit, SimpleGit, SimpleGitOptions } from "simple-git";
import { promises as fs } from "fs";
import path from "path";

// TODO use a lock to avoid race conditions

function getGitOptions(dir: string): Partial<SimpleGitOptions> {
  const options: Partial<SimpleGitOptions> = {
    baseDir: dir,
    binary: "git",
    maxConcurrentProcesses: 6,
    trimmed: false,
  };
  return options;
}

function getGit(dir: string): SimpleGit {
  const options = getGitOptions(dir);
  return simpleGit(options);
}

export async function initGit(dir: string) {
  await fs.mkdir(dir, { recursive: true });
  await fs.mkdir(dir + "/audio/opus", { recursive: true });
  await fs.mkdir(dir + "/audio/mp3", { recursive: true });
  const options = getGitOptions(dir);
  const git: SimpleGit = simpleGit(options);
  await git.init();
}

interface GitCommitFile {
  // source file
  from: string;
  // path relative to the git repo
  // this is where the file should be move before adding and committing
  to: string;
}

export interface GitCommit {
  committer: string;
  email: string;
  message: string;
  files: GitCommitFile[];
}

export async function saveToGit(dir: string, data: GitCommit) {
  if (data.files.length === 0) {
    return;
  }
  const git = getGit(dir);
  const paths = [];
  for (const file of data.files) {
    await fs.rename(file.from, path.join(dir, file.to));
    paths.push(file.to);
  }
  await git.add(paths);
  await git.addConfig("user.name", data.committer);
  await git.addConfig("user.email", data.email);
  await git.commit(data.message);
}
