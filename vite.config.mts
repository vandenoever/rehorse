import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import { VitePWA } from "vite-plugin-pwa";
import replace from "@rollup/plugin-replace";

// https://vitejs.dev/config/
export default defineConfig({
  base: "./",
  build: {
    sourcemap: true,
    outDir: "dist/www",
  },
  server: {
    host: "0.0.0.0",
    proxy: {
      "/api": "http://0.0.0.0:8124",
      "/groups": "http://0.0.0.0:8124",
      "/data": "http://0.0.0.0:8124",
      "/login": "http://0.0.0.0:8124",
      "/ws": {
        target: "ws://0.0.0.0:8124",
        ws: true,
      },
    },
    headers: {
      //'Content-Security-Policy': "worker-src http://localhost:5173 blob: 'self';",
    },
  },
  plugins: [
    vue(),
    VitePWA({
      registerType: "autoUpdate",
      devOptions: {
        enabled: true,
        type: "module",
      },
      workbox: {
        globDirectory: "dist/www",
        globPatterns: [
          "**/*.{js,html,css,png,svg,jpg,ico,webmanifest,otf,ttf,woff2}",
        ],
        maximumFileSizeToCacheInBytes: 2500000,
      },
      // be specific here or the service worker will hijack api requests
      includeAssets: ["assets/**/*"],
      manifest: {
        short_name: "Rehorse",
        name: "Rehorse",
        id: "/rehorse/",
        icons: [
          {
            src: "rehorse-logo.svg",
            type: "image/svg+xml",
            sizes: "32x32",
          },
          {
            src: "pwa-64x64.png",
            sizes: "64x64",
            type: "image/png",
          },
          {
            src: "pwa-192x192.png",
            sizes: "192x192",
            type: "image/png",
          },
          {
            src: "pwa-512x512.png",
            sizes: "512x512",
            type: "image/png",
          },
          {
            src: "maskable-icon-512x512.png",
            sizes: "512x512",
            type: "image/png",
            purpose: "maskable",
          },
        ],
        start_url: ".",
        display: "standalone",
        prefer_related_applications: false,
        theme_color: "red",
      },
    }),
    replace({
      preventAssignment: true,
      __RELOAD_SW__: process.env?.["RELOAD_SW"] === "true" ? "true" : "false",
    }),
  ],
});
