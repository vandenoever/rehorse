import { defineConfig } from "vitest/config";
import vue from "@vitejs/plugin-vue";

export default defineConfig({
  plugins: [
    vue(/*{
      hot: !process.env.VITEST,
      preprocess: [sveltePreprocess({ typescript: true })]
    }*/),
  ],
  test: {
    include: [
      "tests/**/*.{test,spec}.{js,mjs,cjs,ts,mts,cts,vue}",
      "features/**/*.ts",
    ],
    globals: true,
    environment: "jsdom",
    forceRerunTriggers: [
      "**/package.json/**",
      "**/{vitest,vite}.config.*/**",
      "**/features/**/*.feature",
      "**/tests/data/**",
    ],
    setupFiles: ["tests/vitest-setup.ts", "fake-indexeddb/auto"],
    coverage: {
      all: true,
      reporter: ["text", "json", "html"],
    },
    typecheck: {
      enabled: true,
      checker: "vue-tsc",
      tsconfig: "tsconfig.client.json",
    },
  },
});
