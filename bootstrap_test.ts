import { loadFeature, defineFeature } from "jest-cucumber";
import { afterAll, afterEach, beforeAll, beforeEach, expect } from "vitest";

export function bootstrap(file: string) {
  const feature = loadFeature("./" + file);
  return {
    expect,
    defineFeature,
    feature,
    beforeAll,
    afterAll,
    beforeEach,
    afterEach,
  };
}
