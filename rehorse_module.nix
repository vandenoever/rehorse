rehorse: serviceName:
{ config, lib, pkgs, ... }:
let
  cfg = config.services.${serviceName};
  runtimeDeps = [ ];

in with lib; {
  options = {
    services.${serviceName} = {
      enable = mkEnableOption "Run a Rehorse server";

      user = mkOption {
        type = types.str;
        default = serviceName;
        description = "User that runs rehorse.";
      };

      group = mkOption {
        type = types.str;
        default = serviceName;
        description = "Group that runs rehorse.";
      };

      home = mkOption {
        type = types.str;
        default = "/home/${serviceName}";
        description = "Home directory of the rehorse instance.";
      };

      listenAddress = mkOption {
        type = types.str;
        default = "127.0.0.1";
        description = "Address to listen on.";
      };

      listenPort = mkOption {
        type = types.int;
        default = 8124;
        description = "Port to listen on.";
      };
    };
  };
  config = mkIf cfg.enable {
    systemd.services.${serviceName} = {
      wantedBy = [ "multi-user.target" ];
      after = [ "network.target" ];
      description = "Start the ${serviceName} service.";
      path = runtimeDeps;
      environment = { NODE_ENV = "production"; };
      serviceConfig = {
        User = cfg.user;
        Group = cfg.group;
        ExecStart =
          "${rehorse}/bin/rehorse --bind ${cfg.listenAddress} --port ${
            toString cfg.listenPort
          }";
        WorkingDirectory = cfg.home;
        Restart = "on-failure";
        RestartSec = "3";
      };
      unitConfig = {
        StartLimitIntervalSec = 30;
        StartLimitBurst = 3;
      };
      preStart = "";
    };

    environment.systemPackages = [ rehorse ];
    users = {
      users.${cfg.user} = {
        isSystemUser = true;
        description = "${serviceName} daemon user";
        home = cfg.home;
        createHome = true;
        group = cfg.group;
      };
      groups.${cfg.group} = { };
    };
  };
}
