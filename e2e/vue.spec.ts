import { expect, Page } from "@playwright/test";
import { test, logIn, logOut, logInWithForm } from "./utils.js";

test("visits the app root url", async ({ page }) => {
  await page.goto("/");
  const body = page.locator("body");
  await expect(body).toHaveCount(1);
  await expect(page.getByRole("link", { name: "home" })).toHaveCount(1);
  await expect(page.getByRole("link", { name: "Log in" })).toHaveCount(1);
});

test("can visit group overview", async ({ page }) => {
  await page.goto("/");
  await page.getByRole("link", { name: "Assai" }).click();
  for (const name of ["Agenda", "Playlists", "Arrangements", "Recordings"]) {
    await expect(page.getByRole("link", { name })).toHaveCount(1);
  }
});

test("log in", async ({ page }) => {
  await logInWithForm(page);
  await page.getByRole("link", { name: "Wolfgang Amadeus Mozart" }).click();
  await expect(page.getByText("You are logged in")).toHaveText(
    "You are logged in as Wolfgang Amadeus Mozart (wolfgang)",
  );
});

test("log out", async ({ page }) => {
  await logInWithForm(page);
  await logOut(page);
  const login = page.getByRole("button", { name: "Log in" });
  await expect(login).toHaveCount(1);
});

test("log in from saved", async ({ page }) => {
  await logInWithForm(page); // log in so the password is saved
  await logOut(page);
  const savedLogin = page.getByRole("button", { name: "wolfgang" });
  await expect(savedLogin).toHaveCount(1);
  await savedLogin.click();
  await page.getByRole("link", { name: "Wolfgang Amadeus Mozart" }).click();
  await expect(page.getByText("You are logged in")).toHaveText(
    "You are logged in as Wolfgang Amadeus Mozart (wolfgang)",
  );
});

test("can visit group overview2", async ({ page }) => {
  await logIn(page);
  await page.goto("/#/assai");
  for (const name of ["Agenda", "Playlists", "Arrangements", "Recordings"]) {
    await expect(page.getByRole("link", { name })).toHaveCount(1);
  }
});

test("can visit agenda", async ({ page }) => {
  await logIn(page);
  await page.goto("/#/assai/agenda");
  await expect(page.getByRole("button", { name: "Add" })).toHaveCount(2);
});

async function createRecordings(page: Page, files: string[]) {
  test.slow();
  await logIn(page);
  await page.goto("/#/assai/pieces");
  await page.getByRole("button", { name: "Add" }).click();
  await page.locator("input[type='file']").setInputFiles(files);
  await page.getByRole("button", { name: "Upload" }).click();
}

test("create recording", async ({ page }) => {
  await createRecordings(page, ["tests/data/files/audio/k523.opus"]);
  await expect(page.getByText("Tempo")).toHaveText("Tempo:");
});

test("create recordings", async ({ page }) => {
  await createRecordings(page, [
    "tests/data/files/audio/k522-mv1.opus",
    "tests/data/files/audio/k523.opus",
  ]);
  await expect(page.getByText("opus")).toHaveCount(2);
});

test("create arrangement", async ({ page }) => {
  test.slow(); // this test is slow because it loads the pdf viewer
  await logIn(page);
  await page.goto("/#/assai/arrangements");
  await page.getByRole("button", { name: "Add" }).click();
  const title = "Abendempfindung (KV 523)";
  await page.getByRole("textbox", { name: "title" }).fill(title);
  await page
    .locator("input[type='file']")
    .setInputFiles("tests/data/files/pdf/k523-a4.pdf");
  await page.getByRole("button", { name: "Upload" }).click();
  // wait until 'upload' is clickable again
  await page.getByRole("button", { name: "Upload" }).click();
  // select 'tacet' for the vocals
  const vocal = page.locator("select").first();
  await vocal.press("ArrowDown");
  await vocal.press("ArrowDown");
  await vocal.press("ArrowDown");
  await vocal.press("Enter");
  const piano = page.locator("select").nth(1);
  await piano.press("k");
  await piano.press("Enter");
  await page.getByRole("button", { name: "Save" }).click();
  await expect(page.getByRole("heading")).toHaveText(title);
  await expect(page.getByRole("link", { name: "Piano" })).toHaveText("🗋 Piano");
  await expect(page.getByText("Vocal")).toHaveText("🔇 Vocal");
  await page.getByRole("link", { name: "Piano" }).click();
  // wait for the pdf view to load
  await expect(page.locator("div.pdfview")).toBeVisible();
});

test("create new person in arrangement", async ({ page }) => {
  await logIn(page);
  await page.goto("/#/assai/arrangements");
  await page.getByRole("button", { name: "Add" }).click();
  const title = "Abendempfindung (KV 523)";
  const person = "Wolfgang Amadeus Mozart";
  const titleElement = page.getByRole("textbox", { name: "title" });
  await titleElement.fill(title);
  // add a new person
  const composerInput = page.getByRole("textbox").nth(1);
  await expect(composerInput).toHaveAttribute("placeholder", "Composers");
  await composerInput.pressSequentially(person);
  await composerInput.press("Enter");
  // check that this person is available in the arrangers input
  const arrangerInput = page.getByRole("textbox").nth(2);
  await expect(arrangerInput).toHaveAttribute("placeholder", "Arrangers");
  await arrangerInput.press("w");
  await arrangerInput.press("ArrowDown");
  await arrangerInput.press("Enter");
  const parent = page.locator("div.multiselect__tags").nth(1);
  await expect(parent).toHaveText(person);
});

test("create concert", async ({ page }) => {
  await logIn(page);
  await page.goto("/#/assai/agenda");
  await page.getByRole("button", { name: "Add" }).first().click();
  await page.getByRole("textbox", { name: "date" }).fill("2030-10-10");
  await page.getByLabel("Start").fill("10:10");
  await page.getByLabel("End").fill("12:10");
  const locationInput = page.getByRole("textbox", { name: "location" });
  const location = "Pagoda";
  await locationInput.pressSequentially(location);
  await locationInput.press("Enter");
  await page.getByRole("textbox", { name: "remarks" }).fill("usual outfit");
  await expect(page.getByRole("textbox", { name: "remarks" })).toHaveCount(1);
  await page.getByRole("button", { name: "Save" }).click();
  await expect(page.locator(":text('Location') + dd")).toHaveText(location);
  await page.getByRole("link", { name: "agenda" }).click();
  await expect(
    page
      .getByRole("link", { name: "donderdag 10 oktober 2030 om 10:10" })
      .first(),
  ).toHaveCount(1);
});

test("create playlist", async ({ page }) => {
  await logIn(page);
  await page.goto("/#/assai/playlists");
  await page.getByRole("button", { name: "Add" }).first().click();
  const title = "Wolfies favourites";
  await page.getByRole("textbox", { name: "title" }).fill(title);
  const playlistInput = page.getByRole("textbox").nth(1);
  await expect(playlistInput).toHaveCount(1);
  await expect(playlistInput).toHaveAttribute("placeholder", "Arrangements");
  const entry = "Abend";
  await playlistInput.pressSequentially(entry);
  await playlistInput.press("ArrowDown");
  await playlistInput.press("Enter");
  await page.getByRole("button", { name: "Save" }).click();
  await expect(page.getByRole("heading")).toHaveText(title);
  await page.getByRole("link", { name: "playlist" }).click();
  await expect(page.getByRole("link", { name: title }).first()).toHaveCount(1);
});

/*
    This tests hangs after clicking on the login button.
test("can log in", async ({ page }) => {
  await page.goto("/#/user");
  await page.getByRole("textbox", { name: "username" }).fill("wolfgang");
  await page.getByRole("textbox", { name: "password" }).fill("wolfgang");
  await expect(page.getByRole("button", { name: "Log in" })).toHaveCount(1);
  let url;
  page.on("response", (response) => {
    expect(response.status()).toBe(200);
    url = response.url();
    console.log(`<< ${url}`, response);
  });
  await page.getByRole("button", { name: "login" }).click();
  await expect(page.getByText("You are logged in")).toHaveText(
    "Wolfgang Amadeus Mozart",
  );
  expect(url).toBe("hello");
});
*/
