import { test as base, ConsoleMessage, expect, Page } from "@playwright/test";

export interface Options {
  defaultItem: string;
}

// Extend the test to log any errors logged to the console and any pageerrors
// thrown.
export const test = base.extend<Options>({
  page: async ({ page }, use): Promise<void> => {
    const messages: (ConsoleMessage | string)[] = [];
    page.on("console", (msg: ConsoleMessage) => {
      if (msg.type() === "error") {
        messages.push(msg);
      }
    });
    // Uncaught (in promise) TypeError + friends are page errors.
    page.on("pageerror", (error: TypeError) => {
      messages.push(`[${error.name}] ${error.message}`);
    });
    await use(page);
    expect(messages).toStrictEqual([]);
  },
});

export async function logIn(page: Page) {
  const response = await page.request.post("/login", {
    data: {
      username: "wolfgang",
      password: "wolfgang",
    },
  });
  expect(response.status()).toBe(200);
}

export async function logInWithForm(page: Page) {
  await page.goto("/");
  await page.getByRole("link", { name: "Log in" }).click();
  await page.getByRole("textbox", { name: "username" }).fill("wolfgang");
  await page.getByRole("textbox", { name: "password" }).fill("wolfgang");
  const login = page.getByRole("button", { name: "Log in" });
  await expect(login).toHaveCount(1);
  await login.click();
}

export async function logOut(page: Page) {
  await page.getByRole("link", { name: "Wolfgang Amadeus Mozart" }).click();
  await page.goto("/#/user");
  const logout = page.getByRole("button", { name: "Log out" });
  await expect(logout).toHaveCount(1);
  // for some unknown reason, logout.click() does not work because a parent
  // element with class 'level-left' intercepts pointer events
  //   await logout.click();
  await logout.press("Enter");
}
