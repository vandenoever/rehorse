import { test as base, ConsoleMessage, expect, Page } from "@playwright/test";

export interface Options {
  defaultItem: string;
}

// Extend the test to log any errors logged to the console and any pageerrors
// thrown.
export const test = base.extend<Options>({
  page: async ({ page }, use): Promise<void> => {
    const messages: (ConsoleMessage | string)[] = [];
    page.on("console", (msg: ConsoleMessage) => {
      if (msg.type() === "error") {
        messages.push(msg);
      }
    });
    // Uncaught (in promise) TypeError + friends are page errors.
    page.on("pageerror", (error: TypeError) => {
      messages.push(`[${error.name}] ${error.message}`);
    });
    await use(page);
    expect(messages).toStrictEqual([]);
  },
});

export async function logIn(page: Page) {
  const response = await page.request.post("/login", {
    data: {
      username: "wolfgang",
      password: "wolfgang",
    },
  });
  expect(response.status()).toBe(200);
}
