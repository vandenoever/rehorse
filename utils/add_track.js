#!/usr/bin/env node

// nix-shell -p nodejs unixtools.xxd ffmpeg-full
const fs = require("fs");
const path = require("path");
const child_process = require("child_process");
const uuid = require("uuid");
const uuidv4 = uuid.v4;

// read current data
const data = JSON.parse(fs.readFileSync("data/rehorse.json"));
// check that all files exist
for (const [uuid, piece] of Object.entries(data.pieces)) {
  if (!fs.existsSync(path.join("data", piece.url))) {
    console.log(`File ${piece.url} does not exist.`);
  }
}

const files = process.argv.slice(2);
for (const file of files) {
  if (!fs.existsSync(file)) {
    console.log(`File ${file} does not exist.`);
    process.exit(1);
  }
  const basename = path.basename(file);
}
for (const file of files) {
  const basename = path.basename(file);
  const dataPath = path.join("data", basename);
  if (fs.existsSync(dataPath)) {
    console.log(`File ${dataPath} already exists.`);
    process.exit(1);
  }
  fs.copyFileSync(file, dataPath);
  data.pieces[uuidv4()] = {
    title: basename,
    url: basename,
    marks: [
      {
        name: "",
        time: 0,
      },
      {
        name: "",
        time: 100,
      },
    ],
  };
}
fs.writeFileSync("data/rehorse.json", JSON.stringify(data, null, "  "));
child_process.execSync(path.join(process.cwd(), "data", "make_missing.sh"), {
  cwd: "data",
  stdio: "inherit",
});
