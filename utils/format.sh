#!/usr/bin/env bash

find . -name '*.nix' -exec nixfmt {} +
node_modules/.bin/prettier -w --cache .
