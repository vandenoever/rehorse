// This script creates `shared/version.json` which contains the version
// number, git revision and date for the server and web interface.

import package_json from "../package.json";
import { readFileSync, statSync, writeFileSync } from "fs";
import { execSync } from "child_process";

interface VersionInfo {
  // The version is read from package.json.
  // The latest git tag should be the same value with a 'v' prepended.
  version: string;
  // This date is read from the latest git commit. If the repository has
  // uncommitted changes, the current date is used.
  // When no .git folder is present, the value is taken from the environment
  // variable GIT_LATEST_COMMIT_DATE
  lastModified: string;
  // The full sha-1 hash of the latest git commit.
  // When no .git folder is present, the value is taken from the environment
  // variable GIT_LATEST_COMMIT_SHA1
  revision: string;
}

const error = (e: string): never => {
  console.error(e);
  process.exit(1);
};

const trimVersionNumber = (v: string) => {
  const regex = /^v\d+\.\d+\.\d+$/;
  if (!regex.test(v)) {
    error(`"${v}" is not formatted like "v1.1.1".`);
  }
  return v.substring(1);
};

const runCmd = (cmd: string): string => {
  try {
    return execSync(cmd, {
      encoding: "utf-8",
      stdio: ["pipe", "pipe", "ignore"],
    }).trim();
  } catch (e: unknown) {
    error(`Could not run "${cmd}" to get the latest git tag.\n${e}`);
  }
  return "";
};

// go from 20240802201138 to 2024-08-02T20:11:38
const parseCompactDate = (compact: string) => {
  const full =
    compact.substring(0, 4) +
    "-" +
    compact.substring(4, 6) +
    "-" +
    compact.substring(6, 8) +
    "T" +
    compact.substring(8, 10) +
    ":" +
    compact.substring(10, 12) +
    ":" +
    compact.substring(12, 14);
  const date = Date.parse(full);
  if (isNaN(date)) {
    throw new Error(
      `The date '${compact}' expanded to '${full}' is not valid.`,
    );
  }
  return full;
};

const getVersionInfo = (): VersionInfo => {
  const version = package_json.version;
  const gitStats = statSync(".git", { throwIfNoEntry: false });
  let lastmod: string | undefined;
  let revision: string | undefined;
  if (gitStats?.isDirectory()) {
    // If the git repo is shalllow (like in CI), this command fails.
    // `echo ''` then ensures exit code 0 and then output is then an empty string
    const gitOutput = runCmd("git describe --tags --no-abbrev || echo ''");
    if (gitOutput) {
      const gitVersion = trimVersionNumber(gitOutput);
      if (gitVersion !== version) {
        error(
          `The version numbers in package.json (${version}) is not equal to the latest git tag (${gitVersion}).`,
        );
      }
    }
    revision = runCmd("git describe --no-tags --always --no-abbrev --dirty");
    if (revision.endsWith("-dirty")) {
      lastmod = new Date(Date.now()).toISOString();
    } else {
      lastmod = runCmd("git log -1 --format=%cd --date=iso-strict");
    }
  } else if (process.env.GIT_LATEST_COMMIT_DATE) {
    revision = process.env.GIT_LATEST_COMMIT_SHA1;
    lastmod = parseCompactDate(process.env.GIT_LATEST_COMMIT_DATE);
  }
  if (revision === undefined) {
    error("The revision could not be determined.");
  } else if (lastmod === undefined) {
    error("The modification date revision could not be determined.");
  } else {
    return {
      version,
      lastModified: lastmod,
      revision,
    };
  }
};

const writeVersionFile = (info: VersionInfo) => {
  const path = "shared/version.json";
  const json = JSON.stringify(info, null, 2) + "\n";
  try {
    const previousFileContent = readFileSync(path, { encoding: "utf8" });
    if (json === previousFileContent) {
      return;
    }
  } catch (_e: unknown) {
    // ignore
  }
  writeFileSync(path, json, { encoding: "utf8" });
};

const versionInfo = getVersionInfo();
writeVersionFile(versionInfo);
