#!/usr/bin/env node
// nix-shell -p nodejs unixtools.xxd ffmpeg-full

const fs = require("fs");
const path = require("path");

// read current data
const data = JSON.parse(fs.readFileSync("data/rehorse.json"));
const ids = [...Object.keys(data.pieces)];
ids.sort((a, b) => {
  const atitle = data.pieces[a].title.replace(/\W/g, "").toLowerCase();
  const btitle = data.pieces[b].title.replace(/\W/g, "").toLowerCase();
  return atitle.localeCompare(btitle);
});
const pieces = {}; // new Map();
for (id of ids) {
  pieces[id] = data.pieces[id];
}
data.pieces = pieces;
fs.writeFileSync("data/rehorse.json", JSON.stringify(data, null, "  "));
