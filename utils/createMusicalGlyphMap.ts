import { readdirSync, readFileSync, statSync, writeFileSync } from "fs";
import { join as path_join } from "path";
import { execFileSync } from "child_process";
import smuflRanges from "../src/smufl/ranges.json";
import glyphnames from "../src/smufl/glyphnames.json";

function listFonts(): string[] {
  const fontPaths: string[] = [];
  const dirPath = "src/assets/fonts";
  readdirSync(dirPath).forEach((file) => {
    fontPaths.push(path_join(dirPath, file));
  });
  return fontPaths;
}

interface UnicodeRange {
  start: number;
  end: number;
}

function getFontUnicodeRanges(fontPath: string): UnicodeRange[] {
  console.log(`Querying ${fontPath}`);
  const ranges: UnicodeRange[] = [];
  const stdout = execFileSync(
    "fc-query",
    ["--format='%{charset}\n'", fontPath],
    {
      encoding: "utf8",
    },
  );
  for (const range of stdout.replaceAll("'", "").split(" ")) {
    if (range.indexOf("-") !== -1) {
      const [start, end] = range.split("-");
      ranges.push({ start: parseInt(start, 16), end: parseInt(end, 16) });
    } else if (range.length > 0) {
      ranges.push({ start: parseInt(range, 16), end: parseInt(range, 16) });
    }
  }
  return ranges;
}

function codepointInRanges(codepoint: number, ranges: UnicodeRange[]): boolean {
  for (const range of ranges) {
    if (codepoint >= range.start && codepoint <= range.end) {
      return true;
    }
  }
  return false;
}

function codepointInUnicodeRanges(
  codepoint: number,
  ranges: Record<string, UnicodeRange[]>,
): boolean {
  for (const r of Object.values(ranges)) {
    if (codepointInRanges(codepoint, r)) {
      return true;
    }
  }
  return false;
}

interface Glyph {
  t: string;
  desc: string;
}

function getGlyphs(
  ranges: Record<string, UnicodeRange[]>,
): Record<string, Glyph> {
  const glyphs = {};
  for (const [key, glyph] of Object.entries(glyphnames)) {
    const codepoint = parseInt(glyph.codepoint.substring(2), 16);
    if (codepointInUnicodeRanges(codepoint, ranges)) {
      const t = String.fromCodePoint(codepoint);
      glyphs[key] = {
        t,
        desc: glyph.description,
      };
    }
  }
  return glyphs;
}

function getSmuflRanges(
  desiredRanges: string[],
  glyphs: Record<string, Glyph>,
) {
  const ranges = {};
  for (const [key, range] of Object.entries(smuflRanges)) {
    const rangeGlyphs = {};
    for (const r of range.glyphs) {
      if (r in glyphs) {
        rangeGlyphs[r] = glyphs[r];
      }
    }
    if (
      Object.keys(rangeGlyphs).length > 0 &&
      desiredRanges.indexOf(key) !== -1
    ) {
      ranges[key] = { desc: range.description, glyphs: rangeGlyphs };
    }
  }
  return ranges;
}

function getFontsUnicodeRanges(): Record<string, UnicodeRange[]> {
  const fontUnicodeRanges: Record<string, UnicodeRange[]> = {};
  const fontPaths = listFonts();
  for (const path of fontPaths) {
    const ranges = getFontUnicodeRanges(path);
    fontUnicodeRanges[path] = ranges;
  }
  return fontUnicodeRanges;
}

function checkDesiredRanges(ranges: string[]) {
  for (const range of ranges) {
    if (!(range in smuflRanges)) {
      console.error(`'${range}' is not a valid Smufl range`);
      process.exit(1);
    }
  }
}

function run() {
  const desiredRanges = [
    "arrowsAndArrowheads",
    "articulation",
    "barRepeats",
    "barlines",
    "beamsAndSlurs",
    "brassTechniques",
    "clefs",
    "dynamics",
    "electronicMusicPictograms",
    "holdsAndPauses",
    "individualNotes",
    "metronomeMarks",
    "miscellaneousSymbols",
    "octaves",
    "repeats",
    "rests",
    "standardAccidentals12Edo",
    "timeSignatures",
  ];
  checkDesiredRanges(desiredRanges);
  const fontRanges = getFontsUnicodeRanges();
  const glyphs = getGlyphs(fontRanges);
  const ranges = getSmuflRanges(desiredRanges, glyphs);
  const json = JSON.stringify(ranges, null, 2);
  const outPath = "src/musical-glyphs.json";
  console.log(`Writing '${outPath}'`);
  writeFileSync(outPath, json, { encoding: "utf8" });
}
run();
