[![status-badge](https://woodpecker.nlnet.nl/api/badges/vandenoever/rehorse/status.svg)](https://woodpecker.nlnet.nl/vandenoever/rehorse)

# Rehorse

Rehorse is an application for bands and individuals to manage sheet music.

## Development

Install the build requirements with `npm ci`.

Run

```
npm run dev -- --config tests/data/basic/config.json
```

This will start the frontend with `vite` and the backend with `tsx`.

To do full typechecking run `npm run build` or `vue-tsc -b`.

Format the code with `utils/format.sh`.

Run linting with `npm run lint`.

### Preview mode

Run the server with the generated code from `npm run build` instead of with the
Hot Module Replacement from `npm run dev`.

`npm run preview -- --config tests/data/basic/config.json`

### e2e testing

```
xvfb-run playwright test --ui --ui-port=7390 --ui-host=localhost
```
